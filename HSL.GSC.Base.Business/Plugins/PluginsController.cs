﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSL.GSC.API;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace HSL.GSC.Base.Business.Plugins
{
    public class PluginsController
    {
        [ImportMany(typeof(IPlugin))]
        public IEnumerable<IPlugin> Plugins { get; private set; }
        private CompositionContainer container;
        private DirectoryCatalog catalog;
        
        public PluginsController(string pluginsDirectoryPath)
        {
            catalog = new DirectoryCatalog(pluginsDirectoryPath);
            container = new CompositionContainer(catalog, true);
            container.ComposeParts(this);
        }
    }
}
