﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using HSL.GSC.Base.Business.ClientHandling;
using HSL.GSC.Base.Data.MissionInfromation;
using HSL.GSC.Base.Data.SPL;
using HSL.GSC.Base.Data.Storage;
using HSL.GSC.CommunicationFormat;
using HSL.GSC.API;
using HSL.GSC.CommunicationFormat.Packets;
using System.Configuration;

namespace HSL.GSC.Base.Business.Managing
{
    /// <summary>
    /// The class of the End-Node manager
    /// </summary>
    public class EndNodeManager : Manager
    {
        private StorageController storageController;
        private MissionInfromationController missionInfromationController;
        private readonly byte ackServiceType;
        private readonly byte ackServiceSubtype;
        public EndNodeManager(StorageController storageController, MissionInfromationController missionInfromationController, byte ackServiceType, byte ackServiceSubtype) : base()
        {
            this.ackServiceType = ackServiceType;
            this.ackServiceSubtype = ackServiceSubtype;
            this.missionInfromationController = missionInfromationController;
            this.storageController = storageController;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                missionInfromationController = null;
                storageController = null;
            }
        }

        /// <summary>
        /// Treats all of the incoming messages in the queue of every EndNode ClientHandler, implemented for End-Node manager
        /// </summary>
        public override void AddClient(ClientHandler client)
        {
            if (disposed) throw new InvalidOperationException();
            client.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.ManagerAck, client.Guid.ToString())));
            Clients.Add(client);
        }
        protected override void BeginHandlingMessages()
        {
            if (disposed) throw new InvalidOperationException();
            while (!disposed)
            {
                foreach (ClientHandler client in Clients.ToList())
                {
                    if (disposed) break;
                    ActUponMessage(client, client.DequeueReceived());
                    Task.Delay(100).Wait();
                }
            }
        }
        protected override void ActUponMessage(ClientHandler sender, ExternalMessage message)
        {
            if (disposed) throw new InvalidOperationException();
            if (message == null) return;
            if (message.Type == MessageTypeEnum.RawTelemetry)
            {
                byte[] encodedPacket = (byte[])message.Content;
                TelemetryPacket packet = new TelemetryPacket(encodedPacket);
                byte ackServiceType = byte.Parse(ConfigurationManager.AppSettings.Get("AckServiceType")),
                    ackServiceSubtype = byte.Parse(ConfigurationManager.AppSettings.Get("AckServiceSubtype"));
                
                if (packet.ServiceType == ackServiceType &&
                    packet.ServiceSubtype == ackServiceSubtype)
                {
                    ActUponAck(packet.Data);
                    storageController.AddTelemetryPacket(packet);
                    return;
                }
                /*foreach (var plugin in Plugins.PluginsController.Plugins)
                {
                    if (packet.ServiceType == plugin.ServiceType && packet.ServiceSubtype == plugin.ServiceSubtype)
                    {
                        InteractWithPlugin(plugin, packet);
                        return;
                    }
                }*/
                storageController.AddTelemetryPacket(packet);
            }
        }

        public void ActUponAck(byte[] ack)
        {
            DecipheredPacket decipheredAckFormat = missionInfromationController.FindCertainPacket(this.ackServiceType, this.ackServiceSubtype, true);
            DecipheredPacket decipheredAck = StorageController.Decipher(decipheredAckFormat, ack);

            if (storageController is DatabaseController)
            {
                uint commandId = uint.Parse(decipheredAck.Parameters[0].Data);
                if (commandId >= 1000)
                    (storageController as DatabaseController).AcknowledgeTelecommand(commandId, decipheredAck.Parameters[1] as BitmapParameter);
            }

            byte errorType = (decipheredAck.Parameters[2] as BitmapParameter).BitFields[0].optionValue;
        }

        public void InteractWithPlugin(IPlugin plugin, IRawTelemetry telemetry)
        {
            if (!(storageController is DatabaseController))
                throw new Exception("Storage controller isn't appropriate for plugins");

            var requiredPacketInfo = plugin.DecipherAndRequire(telemetry, out IDecipheredPart[] parts);
            IDatabaseData requiredPacket;
            try
            {
                requiredPacket = (storageController as DatabaseController).FindScientificPacket(requiredPacketInfo);
            }
            catch (InvalidOperationException)
            {
                requiredPacket = (storageController as DatabaseController).AddScientificPacket(requiredPacketInfo);
            }
            var filledPacket = plugin.InsertParts(requiredPacket, parts);
            (storageController as DatabaseController).EditScientificPacket(filledPacket);
        }
        public IDatabaseData DecipherAndRequire(IPlugin plugin, IRawTelemetry telemetry, out IDecipheredPart[] parts)
        {
            var toReturn = plugin.DecipherAndRequire(telemetry, out IDecipheredPart[] partsToReturn);
            parts = partsToReturn;
            return toReturn;
        }
        public byte[] DecipherAndSave(IPlugin plugin, IRawTelemetry telemetry)
        {
            if (!(storageController is DatabaseController))
                throw new Exception("Storage controller isn't appropriate for plugins");

            var requiredPacketInfo = plugin.DecipherAndRequire(telemetry, out IDecipheredPart[] parts);
            IDatabaseData requiredPacket;
            try
            {
                requiredPacket = (storageController as DatabaseController).FindScientificPacket(requiredPacketInfo);
            }
            catch (InvalidOperationException)
            {
                requiredPacket = (storageController as DatabaseController).AddScientificPacket(requiredPacketInfo);
            }
            var filledPacket = plugin.InsertParts(requiredPacket, parts);
            (storageController as DatabaseController).EditScientificPacket(filledPacket);
            return filledPacket.Data;
        }
        
        public bool SendMessage(InternalMessage message)
        {
            foreach (ClientHandler client in Clients.ToList())
            {
                if (message.DestinationGuid == client.Guid)
                {
                    client.EnqueueOutgoing(new ExternalMessage(message));
                    return true;
                }
            }
            return false;
        }

        public bool SendCommand(DecipheredPacket command, string guid)
        {
            var packetToSend = new TelecommandPacket(default(uint), command.ServiceType, command.ServiceSubtype, default(DateTime), (byte[])command.TotalRawData.Clone());
            IStorageElement databasePacket = storageController.AddTelecommandPacket(packetToSend);
            packetToSend = new TelecommandPacket(databasePacket);
            bool wasSent = SendMessage(new InternalMessage(new Guid(guid), MessageTypeEnum.RawTelecommand, packetToSend.Encode()));
            return wasSent;
        }

        public bool StreamCommand(DecipheredPacket command)
        {
            bool success = true;
            foreach (var client in this.Clients)
            {
                if (!SendCommand(command, client.Guid.ToString()))
                    success = false;
            }
            return success;
        }
    }
}
