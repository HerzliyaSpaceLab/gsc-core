﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HSL.GSC.CommunicationFormat;
using HSL.GSC.Base.Business.ClientHandling;
using System.Threading.Tasks;
using System.Threading;

namespace HSL.GSC.Base.Business.Managing
{
    /// <summary>
    /// An abstraction for the communication managers
    /// </summary>
    public abstract class Manager : IDisposable
    {
        protected volatile bool disposed = false;
        private Task managerTask;
        /// <summary>
        /// List of all the clients of the similar to manager type
        /// </summary>
        protected List<ClientHandler> Clients { get; set; }
        /// <summary>
        /// The main constructor of a communication manager
        /// </summary>
        protected Manager()
        {
            Clients = new List<ClientHandler>();
            managerTask = new Task(BeginHandlingMessages);
        }
        public void Start()
        {
            managerTask.Start();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                disposed = true;
                if (managerTask != null)
                {
                    managerTask.Wait();
                    managerTask.Dispose();
                    managerTask = null;
                }
                if (Clients != null)
                {
                    foreach (var client in Clients)
                    {
                        if (client != null)
                        {
                            client.Dispose();
                        }
                    }
                    Clients.Clear();
                    Clients = null;
                }
            }
        }
        ~Manager()
        {
            Dispose(false);
        }
        /// <summary>
        /// The abstract method of treating an incoming message
        /// </summary>
        public abstract void AddClient(ClientHandler client);
        protected abstract void BeginHandlingMessages();
        protected abstract void ActUponMessage(ClientHandler sender, ExternalMessage message);
    }
}
