﻿using HSL.GSC.Base.Business.ClientHandling;
using HSL.GSC.CommunicationFormat;
using HSL.GSC.CommunicationFormat.Answers;
using HSL.GSC.CommunicationFormat.MissionFormat;
using HSL.GSC.CommunicationFormat.Packets;
using HSL.GSC.CommunicationFormat.Requests;
using HSL.GSC.Base.Data.MissionInfromation;
using HSL.GSC.Base.Data.SPL;
using HSL.GSC.Base.Data.Storage;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HSL.GSC.Base.Business.Managing
{
    /// <summary>
    /// The class of the GUI manager
    /// </summary>
    public sealed class GUIManager : Manager
    {
        private EndNodeManager endNodeManager;
        private MissionInfromationController missionInfromationController;
        private DatabaseController databaseController;
        public bool IsThereController
        {
            get
            {
                return Clients.Any(x => x.ClientIdentity.Type == ClientIdentity.ClientType.Controller);
            }
        }

        public GUIManager(EndNodeManager endNodeManager, MissionInfromationController missionInfromationController, DatabaseController databaseController) : base()
        {
            this.endNodeManager = endNodeManager;
            this.missionInfromationController = missionInfromationController;
            this.databaseController = databaseController;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                endNodeManager = null;
                missionInfromationController = null;
                databaseController = null;
            }
        }

        /// <summary>
        /// The method of treating all of the incoming messages in the queue, implemented for GUI manager
        /// </summary>
        public override void AddClient(ClientHandler client)
        {
            if (disposed) throw new InvalidOperationException();
            client.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.ManagerAck, "You connected to the manager successfully")));

            if (client.ClientIdentity.Type == ClientIdentity.ClientType.GUI)
            {
                Debug.WriteLine(client.Guid + " GUI joined");
            }
            else if (client.ClientIdentity.Type == ClientIdentity.ClientType.Controller)
            {
                Debug.WriteLine(client.Guid + " controller joined");
            }
            client.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.TelemetryMissionFormat, missionInfromationController.FindFullMissionFormat(true)));
            client.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.TelecommandMissionFormat, missionInfromationController.FindFullMissionFormat(false)));

            Clients.Add(client);
            Debug.WriteLine(client.Guid + " received handshake information");
        }


        protected override void BeginHandlingMessages()
        {
            if (disposed) throw new InvalidOperationException();
            while (!disposed)
            {
                foreach (ClientHandler client in Clients.ToList())
                {
                    if (disposed) break;
                    if (client.Disposed)
                    {
                        Clients.Remove(client);
                        continue;
                    }
                    ExternalMessage receivedMessage = null;
                    receivedMessage = client.DequeueReceived();
                    ActUponMessage(client, receivedMessage);
                    Task.Delay(100).Wait();
                }
            }
        }

        #region Actions Upon Messages
        protected override void ActUponMessage(ClientHandler sender, ExternalMessage message)
        {
            if (message == null) return;
            if (message.Type == MessageTypeEnum.SystemRequest)
            {
                ExternalMessage outgoingMessage = null;
                if (message.Content is RequestGroup)
                {
                    RequestGroup group = (RequestGroup)message.Content;
                    try
                    {
                        outgoingMessage = ActUponRequestGroup(group);
                    }
                    catch (InvalidOperationException)
                    {
                        sender.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.Error, "Invalid request group sent")));
                        return;
                    }
                }
                else
                {
                    Request request = (Request)message.Content;
                    try
                    {
                        outgoingMessage = ActUponRequest(request);
                    }
                    catch (InvalidOperationException)
                    {
                        sender.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.Error, "Invalid request sent")));
                        return;
                    }
                }
                
                sender.EnqueueOutgoing(outgoingMessage);
            }
            else if (message.Type == MessageTypeEnum.SystemAck)
            {
                SystemAck ack = (SystemAck)message.Content;
                if (ack.type == SystemAck.SystemAckType.Disconnection)
                {
                    Debug.WriteLine(sender.Guid + " disconnected");
                    sender.Dispose();
                }
            }
            else if (message.Type == MessageTypeEnum.TelemetryMissionFormat)
            {
                ServiceTypeFormat[] models = (ServiceTypeFormat[])message.Content;
                missionInfromationController.UpdateFullMissionFormat(true, models);
            }
        }

        #region Requests
        private ExternalMessage ActUponRequestGroup(RequestGroup group)
        {
            int count = group.GetRequestsCount();
            AnswerGroup answerGroup = new AnswerGroup(new Guid(group.GroupId));
            for (int idx = 0; idx < count; idx++)
            {
                answerGroup.AddRequest(AnswerUponRequest(group.GetNextRequest()));
            }
            return new ExternalMessage(MessageTypeEnum.RequestAnswer, answerGroup);
        }

        private ExternalMessage ActUponRequest<T>(T request) where T : Request
        {
            ExternalMessage messageToSend = null;
            if (request is CommandRequest)
            {
                messageToSend = new ExternalMessage(MessageTypeEnum.SystemAck, AnswerUponCommandRequest(request as CommandRequest));
            }
            else
            {
                messageToSend = new ExternalMessage(MessageTypeEnum.RequestAnswer, AnswerUponRequest(request));
            }
            return messageToSend;
        }

        private Answer AnswerUponRequest<T>(T request) where T : Request
        {
            if (request is DecipheredPacketsRequest)
            {
                return AnswerUponRequest(request as DecipheredPacketsRequest);
            }
            else if (request is StandardPacketsRequest)
            {
                return AnswerUponRequest(request as StandardPacketsRequest);
            }
            else
            {
                throw new InvalidOperationException("Unknown request");
            }
        }

        private Answer AnswerUponRequest(StandardPacketsRequest request)
        {
            foreach (StandardPacketsRequest.ParameterType parameter in Enum.GetValues(typeof(StandardPacketsRequest.ParameterType)))
            {
                if (request.ParameterTypes.Where(x => x == parameter).Take(2).Count() > 1)
                {
                    throw new Exception();
                }
            }

            bool isThereLength = request.ParameterTypes.Any(x => x == StandardPacketsRequest.ParameterType.Length);
            bool isThereStartTime = request.ParameterTypes.Any(x => x == StandardPacketsRequest.ParameterType.StartTime);
            bool isThereFinalTime = request.ParameterTypes.Any(x => x == StandardPacketsRequest.ParameterType.FinalTime);
            bool isThereMoment = request.ParameterTypes.Any(x => x == StandardPacketsRequest.ParameterType.Moment);

            object packetsArray = null;
            if (isThereStartTime && isThereFinalTime && !isThereLength && !isThereMoment)
            {
                DateTime startTime = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == StandardPacketsRequest.ParameterType.StartTime)];
                DateTime finalTime = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == StandardPacketsRequest.ParameterType.FinalTime)];
                if (request.TelemetryOrTelecommand)
                {
                    packetsArray = databaseController.QueryStandardTelemetryPackets(startTime, finalTime);
                }
                else
                {
                    packetsArray = databaseController.QueryStandardTelecommandPackets(startTime, finalTime);
                }
            }
            else if (isThereLength && isThereMoment && !isThereStartTime && !isThereFinalTime)
            {
                int length = (int)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == StandardPacketsRequest.ParameterType.Length)];
                DateTime moment = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == StandardPacketsRequest.ParameterType.Moment)];
                if (request.TelemetryOrTelecommand)
                {
                    packetsArray = databaseController.QueryStandardTelemetryPackets(length, moment);
                }
                else
                {
                    packetsArray = databaseController.QueryStandardTelecommandPackets(length, moment);
                }
            }
            else
            {
                throw new Exception();
            }

            if (request.TelemetryOrTelecommand)
            {
                return new StandardTelemetryAnswer(new Guid(request.RequestId), packetsArray as StandardTelemetry[]);
            }
            else
            {
                return new StandardTelecommandAnswer(new Guid(request.RequestId), packetsArray as StandardTelecommand[]);
            }
        }

        private Answer AnswerUponRequest(DecipheredPacketsRequest request)
        {
            var c = Stopwatch.StartNew();
            foreach (DecipheredPacketsRequest.ParameterType parameter in Enum.GetValues(typeof(DecipheredPacketsRequest.ParameterType)))
            {
                if (request.ParameterTypes.Where(x => x == parameter).Take(2).Count() > 1)
                {
                    throw new Exception();
                }
            }

            bool isThereLength = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.Length);
            bool isThereMoment = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.Moment);
            bool isThereStartTime = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.StartTime);
            bool isThereFinalTime = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.FinalTime);
            bool isThereServiceType = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.ServiceType);
            bool isThereServiceSubtype = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.ServiceSubtype);

            DecipheredPacket[] packets = null;

            c.Stop();
            c.Restart();

            if (isThereLength)
            {
                int length = (int)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.Length)];
                if (!isThereMoment && !isThereStartTime && !isThereFinalTime && !isThereServiceType && !isThereServiceSubtype)
                {
                    packets = databaseController.QueryDecipheredPackets(missionInfromationController, length, request.TelemetryOrTelecommand);
                }
                else if (isThereMoment && isThereServiceType && isThereServiceSubtype && !isThereStartTime && !isThereFinalTime)
                {
                    DateTime moment = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.Moment)];
                    byte serviceType = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceType)];
                    byte serviceSubtype = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceSubtype)];
                    packets = databaseController.QueryDecipheredPackets(missionInfromationController, length, moment, serviceType, serviceSubtype, request.TelemetryOrTelecommand);
                }
                else
                {
                    throw new Exception();
                }
            }
            else
            {
                if (isThereStartTime && isThereFinalTime && isThereServiceType && isThereServiceSubtype && !isThereMoment)
                {
                    DateTime startTime = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.StartTime)];
                    DateTime finalTime = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.FinalTime)];
                    byte serviceType = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceType)];
                    byte serviceSubtype = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceSubtype)];
                    packets = databaseController.QueryDecipheredPackets(missionInfromationController, startTime, finalTime, serviceType, serviceSubtype, request.TelemetryOrTelecommand);
                }
                else if (!isThereStartTime && !isThereFinalTime && !isThereServiceType && !isThereServiceSubtype && !isThereMoment)
                {
                    packets = databaseController.QueryDecipheredPackets(missionInfromationController, request.TelemetryOrTelecommand);
                }
                else
                {
                    throw new Exception();
                }
            }
            c.Stop();

            return new DecipheredPacketsAnswer(request.TelemetryOrTelecommand, new Guid(request.RequestId), packets);
        }

        private SystemAck AnswerUponCommandRequest(CommandRequest request)
        {
            if (request.Command.TelemetryOrTelecommand)
            {
                throw new Exception();
            }
            bool wasSent = endNodeManager.SendCommand(request.Command, request.EndNode);
            if (wasSent)
            {
                return new SystemAck(SystemAck.SystemAckType.DoneOperation, "The command was successfully sent to the End-Node.");
            }
            else
            {
                return new SystemAck(SystemAck.SystemAckType.Error, "An End-Node with such GUID wasn't found.");
            }
        }

        #region Unused Parameters
        /*private ExternalMessage ActUponParametersRequest(DecipheredPacketsRequest request)
        {
            foreach (DecipheredPacketsRequest.ParameterType parameter in Enum.GetValues(typeof(DecipheredPacketsRequest.ParameterType)))
            {
                if (request.ParameterTypes.Where(x => x == parameter).Take(2).Count() > 1)
                {
                    throw new Exception();
                }
            }

            bool isThereLength = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.Length);
            bool isThereMoment = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.Moment);
            bool isThereId = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.ParameterId);
            bool isThereStartTime = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.StartTime);
            bool isThereFinalTime = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.FinalTime);
            bool isThereServiceType = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.ServiceType);
            bool isThereServiceSubtype = request.ParameterTypes.Any(x => x == DecipheredPacketsRequest.ParameterType.ServiceSubtype);

            ExternalMessage messageToSend = null;

            if (isThereLength)
            {
                int length = (int)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.Length)];
                if (!isThereMoment && !isThereStartTime && !isThereFinalTime && !isThereId && !isThereServiceType && !isThereServiceSubtype)
                {
                    Parameter[][] parameters = null;
                    parameters = databaseController.FindParameters(length, request.telemetryOrTelecommand);
                    if (request.telemetryOrTelecommand)
                    {
                        messageToSend = new ExternalMessage(MessageTypeEnum.TelemetryParameters, parameters);
                    }
                    else
                    {
                        messageToSend = new ExternalMessage(MessageTypeEnum.TelecommandParameters, parameters);
                    }
                }
                else if (isThereMoment && isThereServiceType && isThereServiceSubtype && isThereId && !isThereStartTime && !isThereFinalTime)
                {
                    Parameter[][] parameters = null;
                    DateTime moment = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.Moment)];
                    byte serviceType = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceType)];
                    byte serviceSubtype = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceSubtype)];
                    uint id = (uint)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ParameterId)];
                    parameters = DatabaseController.FindParameters(length, moment, serviceType, serviceSubtype, id, request.telemetryOrTelecommand);
                    if (request.telemetryOrTelecommand)
                    {
                        messageToSend = new ExternalMessage(MessageTypeEnum.DecipheredTelemetry, parameters);
                    }
                    else
                    {
                        messageToSend = new ExternalMessage(MessageTypeEnum.DecipheredTelecommand, parameters);
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            else
            {
                if (isThereStartTime && isThereFinalTime && isThereServiceType && isThereServiceSubtype && isThereId && !isThereMoment)
                {
                    Parameter[][] parameters = null;
                    DateTime startTime = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.StartTime)];
                    DateTime finalTime = (DateTime)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.FinalTime)];
                    byte serviceType = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceType)];
                    byte serviceSubtype = (byte)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ServiceSubtype)];
                    uint id = (uint)request.ParameterValues[request.ParameterTypes.FindIndex(x => x == DecipheredPacketsRequest.ParameterType.ParameterId)];
                    parameters = DatabaseController.FindParameters(startTime, finalTime, serviceType, serviceSubtype, id, request.telemetryOrTelecommand);
                    if (request.telemetryOrTelecommand)
                    {
                        messageToSend = new ExternalMessage(MessageTypeEnum.DecipheredTelemetry, parameters);
                    }
                    else
                    {
                        messageToSend = new ExternalMessage(MessageTypeEnum.DecipheredTelecommand, parameters);
                    }
                }
                else
                {
                    throw new Exception();
                }
            }

            return messageToSend;
        }*/
        #endregion

        #endregion

        #endregion

    }
}
