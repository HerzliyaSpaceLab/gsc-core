﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using HSL.GSC.CommunicationFormat;
using System.Diagnostics;

namespace HSL.GSC.Base.Business.ClientHandling
{
    /// <summary>
    /// Fully automates reception and sending actions of a <see cref="System.Net.Sockets.TcpClient"/>, and provides information about the <see cref="System.Net.Sockets.TcpClient"/> object itself.
    /// </summary>
    public sealed class ClientHandler : IDisposable
    {
        private static object receivedLocker = new object();
        private static object outgoingLocker = new object();
        private static BinaryFormatter formatter = new BinaryFormatter();
        /// <summary>
        /// The type of the <see cref="ClientHandler"/>. Assigned as a <see cref="bool"/> value, because there are only two types of <see cref="ClientHandler"/> objects.
        /// </summary>
        /// <value>
        /// 0 = EndNode
        /// 1 = GUI
        /// </value>
        public ClientIdentity ClientIdentity { get; private set; }
        private Task receiveTask;
        private Task sendTask;
        private volatile bool disposed = false;
        private object disposedLocker = new object();
        public bool Disposed
        {
            get
            {
                lock (disposedLocker)
                {
                    return disposed;
                }
            }
        }
        /// <summary>
        /// An easter egg
        /// </summary>
        public bool DankMode { get; private set; }
        /// <summary>
        /// The <see cref="System.Guid"/> of the <see cref="ClientHandler"/> to identify it
        /// </summary>
        public Guid Guid { get; private set; }
        /// <summary>
        /// The <see cref="System.Net.Sockets.TcpClient"/> itself
        /// </summary>
        private TcpClient TcpClient { get; set; }
        /// <summary>
        /// Received <see cref="ExternalMessage"/> objects from <see cref="TcpClient"/>
        /// </summary>
        public Queue<ExternalMessage> ReceivedMessages { get; set; }
        /// <summary>
        /// <see cref="ExternalMessage"/> objects to be sent to <see cref="TcpClient"/>
        /// </summary>
        private Queue<ExternalMessage> OutgoingMessages { get; set; }
        /// <summary>
        /// The main constructor of <see cref="ClientHandler"/>
        /// </summary>
        public ClientHandler(TcpClient client)
        {
            this.DankMode = false;
            this.Guid = Guid.NewGuid();
            this.TcpClient = client;
            this.ReceivedMessages = new Queue<ExternalMessage>();
            this.OutgoingMessages = new Queue<ExternalMessage>();
            HandshakeAttempt();
            receiveTask = ReceiveAsync();
            sendTask = SendAsync();
        }
        public void Dispose()
        {
            if (disposed) return;
            disposed = true;
            if (receiveTask != null)
            {
                receiveTask.Wait();
                receiveTask.Dispose();
                receiveTask = null;
            }
            if (sendTask != null)
            {
                sendTask.Wait();
                sendTask.Dispose();
                sendTask = null;
            }
            //while (receiveTask.Status != TaskStatus.RanToCompletion || sendTask.Status != TaskStatus.RanToCompletion) ;
            TcpClient.Close();
            TcpClient.Dispose();
        }
        /// <summary>
        /// Dequeues an <see cref="ExternalMessage"/> from <see cref="ReceivedMessages"/>.
        /// </summary>
        public ExternalMessage DequeueReceived()
        {
            lock (receivedLocker)
            {
                if (!ReceivedMessages.Any()) return null;
                return ReceivedMessages.Dequeue();
            }
        }
        public ExternalMessage PeekReceived()
        {
            lock (receivedLocker)
            {
                if (!ReceivedMessages.Any()) return null;
                return ReceivedMessages.Peek();
            }
        }
        /// <summary>
        /// Enqueues an <see cref="ExternalMessage"/> to <see cref="OutgoingMessages"/>.
        /// </summary>
        public void EnqueueOutgoing(ExternalMessage message)
        {
            lock (outgoingLocker)
            {
                if (message == null) return;
                OutgoingMessages.Enqueue(message);
            }
        }
        /// <summary>
        /// Automates the <see cref="ClientIdentity"/> request and sending of <see cref="ExternalMessage"/> with <see cref="MessageTypeEnum.ServerAck"/> type.
        /// </summary>
        private void HandshakeAttempt()
        {
            ExternalMessage message = new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.ServerAck, "You are connected to Michael's hell... I mean server successfully"));
            formatter.Serialize(this.TcpClient.GetStream(), message);
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (true)
            {
                if (TcpClient.GetStream().DataAvailable)
                {
                    try
                    {
                        message = (ExternalMessage)formatter.Deserialize(this.TcpClient.GetStream());
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    if (message.Type == MessageTypeEnum.ClientIdentity)
                    {
                        ClientIdentity = (ClientIdentity)message.Content;
                        return;
                    }
                }
                if (stopwatch.ElapsedMilliseconds >= 3500)
                {
                    stopwatch.Stop();
                    message = new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.ServerKick, "GSC-Base server connection timeout"));
                    try
                    {
                        var stream = this.TcpClient.GetStream();
                        formatter.Serialize(stream, message);
                    }
                    catch (System.IO.IOException)
                    {
                    }
                    throw new TimeoutException();
                }
            }
        }
        /// <summary>
        /// Automates the reception of <see cref="ExternalMessage"/> objects from <see cref="TcpClient"/> and adds them to <see cref="ReceivedMessages"/>.
        /// </summary>
        private async Task ReceiveAsync()
        {
            if (disposed) throw new InvalidOperationException();
            while (!disposed)
            {
                lock (receivedLocker)
                {
                    if (disposed) continue;
                    if (TcpClient.GetStream().DataAvailable)
                    {
                        ExternalMessage message;
                        try
                        {
                            message = (ExternalMessage)formatter.Deserialize(TcpClient.GetStream());
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                        ReceivedMessages.Enqueue(message);
                    }
                }
                await Task.Delay(200);
            }
        }
        /// <summary>
        /// Automates the sending of <see cref="ExternalMessage"/> objects from <see cref="OutgoingMessages"/> to <see cref="TcpClient"/>.
        /// </summary>
        private async Task SendAsync()
        {
            if (disposed) throw new InvalidOperationException();
            while (!disposed)
            {
                lock (outgoingLocker)
                {
                    if (disposed) continue;
                    if (OutgoingMessages.Any())
                    {
                        ExternalMessage message = OutgoingMessages.Dequeue();
                        formatter.Serialize(TcpClient.GetStream(), message);
                    }
                }
                await Task.Delay(200);
            }
        }
    }
}
