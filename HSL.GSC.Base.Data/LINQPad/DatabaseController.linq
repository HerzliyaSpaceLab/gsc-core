<Query Kind="Program">
  <Connection>
    <ID>17e6397f-f18c-4313-81cf-b4b78b582019</ID>
    <Persist>true</Persist>
    <Driver>LinqToSql</Driver>
    <Server>(localdb)\MSSQLLocalDB</Server>
    <CustomAssemblyPath>D:\Main\Satellites\My Real Work\GSC\Code\Base\DataTier\bin\Debug\GSC-Base-DataTier.dll</CustomAssemblyPath>
    <CustomTypeName>DataTier.Database.LINQtoSQLDataContext</CustomTypeName>
    <AttachFile>true</AttachFile>
    <AttachFileName>D:\Main\Satellites\My Real Work\GSC\Code\Base\Tray\bin\Debug\Database\Database.mdf</AttachFileName>
  </Connection>
  <Reference Relative="..\..\CommFormat\bin\Debug\GSC-CommFormat.dll">D:\Main\Satellites\My Real Work\GSC\Code\Base\CommFormat\bin\Debug\GSC-CommFormat.dll</Reference>
  <Namespace>CommFormat.Packets</Namespace>
</Query>

void Main()
{
	this.ObjectTrackingEnabled = false;
	FindTelemetryPackets(new DateTime(2017, 06, 01), new DateTime(2018, 06, 01)).Dump("StartFinalTelemetries");
	FindTelecommandPackets(new DateTime(2017, 06, 01), new DateTime(2018, 06, 01)).Dump("StartFinalTelecommands");
	FindTelemetryPackets(-100, new DateTime(2018, 06, 01)).Dump("MomentTelemetries");
	FindTelecommandPackets(-100, new DateTime(2018, 06, 01)).Dump("MomentTelecommands");
}

IEnumerable<StandardTelemetry> FindTelemetryPackets(DateTime start, DateTime final)
{
	var neededData = new List<StandardTelemetry>();
    foreach (var packet in this.Telemetries)
    {
        if (packet.GroundDateTime <= start) break;
        if (packet.GroundDateTime < final)
            neededData.Add((StandardTelemetry)packet);
    }
	return neededData;
}

IEnumerable<StandardTelecommand> FindTelecommandPackets(DateTime start, DateTime final)
{
	var neededData = new List<StandardTelecommand>();
    foreach (var packet in this.Telecommands)
    {
        if (packet.GroundDateTime <= start) break;
        if (packet.GroundDateTime < final)
            neededData.Add((StandardTelecommand)packet);
    }
	return neededData;
}

IEnumerable<StandardTelemetry> FindTelemetryPackets(int length, DateTime moment)
{
	if (length == 0) throw new ArgumentException("Length is equal to 0", "length");
	var neededData = new List<StandardTelemetry>();
    if (length > 0)
    {
    	foreach (var packet in this.Telemetries)
    	{
        	if (packet.GroundDateTime <= moment) break;
        	neededData.Add((StandardTelemetry)packet);
    	}
        return neededData.Skip(neededData.Count() - length);
    }
    else
    {
    	foreach (var packet in this.Telemetries)
    	{
        	if (neededData.Count == -1 * length) break;
        	if (packet.GroundDateTime < moment)
            	neededData.Add((StandardTelemetry)packet);
    	}
        return neededData;
    }
}

IEnumerable<StandardTelecommand> FindTelecommandPackets(int length, DateTime moment)
{
	if (length == 0) throw new ArgumentException("Length is equal to 0", "length");
	var neededData = new List<StandardTelecommand>();
    if (length > 0)
    {
    	foreach (var packet in this.Telecommands)
    	{
        	if (packet.GroundDateTime <= moment) break;
        	neededData.Add((StandardTelecommand)packet);
    	}
        return neededData.Skip(neededData.Count() - length);
    }
    else
    {
    	foreach (var packet in this.Telecommands)
    	{
        	if (neededData.Count == -1 * length) break;
        	if (packet.GroundDateTime < moment)
            	neededData.Add((StandardTelecommand)packet);
    	}
        return neededData;
    }
}