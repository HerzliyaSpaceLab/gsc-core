﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using HSL.GSC.CommunicationFormat;
using HSL.GSC.CommunicationFormat.MissionFormat;
using HSL.GSC.CommunicationFormat.Packets;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using System.Threading;

namespace HSL.GSC.Base.Data.MissionInfromation
{
    /// <summary>
    /// 
    /// </summary>
    public class MissionInfromationController
    {
        /// <summary>
        /// XML document base with mission information
        /// </summary>
        private XDocument MIBXML;

        /// <summary>
        /// Path where <see cref="MIBXML"/> is saved
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Main controller constructor
        /// </summary>
        /// <param name="filePath">An attribute for <see cref="FileName"/></param>
        public MissionInfromationController(string filePath)
        {
            this.FileName = filePath ?? throw new NullReferenceException();
            MIBXML = XDocument.Load(this.FileName);
        }

        private bool CheckFile(XDocument file)
        {
            // Root

            if (file.Root.Name.LocalName.ToLowerInvariant() != "gscmib" || !file.Root.HasElements || file.Root.HasAttributes
                || file.Root.IsEmpty)
            {
                return false;
            }

            // Main Categories

            XElement telemetry, telecommand, telemetrycals, telecommandcals;
            try
            {
                telemetry = file.Root.Elements().Single(x => x.Name.LocalName.ToLowerInvariant() == "telemetry");
                telecommand = file.Root.Elements().Single(x => x.Name.LocalName.ToLowerInvariant() == "telecommand");
                telemetrycals = file.Root.Elements().Single(x => x.Name.LocalName.ToLowerInvariant() == "telemetrycals");
                telecommandcals = file.Root.Elements().Single(x => x.Name.LocalName.ToLowerInvariant() == "telecommandcals");
            }
            catch (InvalidOperationException)
            {
                return false;
            }

            // Service Types

            if (!telemetry.Elements().All(x => x.Name.LocalName.ToLowerInvariant() == "servicetype") ||
                !telecommand.Elements().All(x => x.Name.LocalName.ToLowerInvariant() == "servicetype"))
            {
                return false;
            }

            IEnumerable<XElement> serviceTypes = telemetry.Elements().Intersect(telecommand.Elements());

            foreach (XElement serviceType in serviceTypes)
            {
                if (!CheckService(serviceType))
                {
                    return false;
                }
            }

            // Service Subtypes

            if (!serviceTypes.Elements().All(
                x => x.Name.LocalName.ToLowerInvariant() == "servicesubtype"
            ))
            {
                return false;
            }

            IEnumerable<XElement> serviceSubtypes = serviceTypes.Elements();

            foreach (XElement serviceSubtype in serviceSubtypes)
            {
                if (!CheckService(serviceSubtype))
                {
                    return false;
                }
            }

            // Parameters

            if (!serviceSubtypes.Elements().All(
                x => x.Name.LocalName.ToLowerInvariant() == "parameter"
            ))
            {
                return false;
            }

            IEnumerable<XElement> parameters = serviceSubtypes.Elements();

            foreach (var parameter in parameters)
            {
                if (!CheckParameter(parameter))
                {
                    return false;
                }
            }

            // Final Conclusion

            return true;
        }

        private bool CheckService(XElement service)
        {
            var attributes = service.Attributes();
            if (attributes.Count() != 2)
            {
                return false;
            }

            XAttribute value, name;
            try
            {
                value = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "value");
                name = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "name");
            }
            catch (InvalidOperationException)
            {
                return false;
            }

            if (!byte.TryParse(value.Value, out byte b))
            {
                return false;
            }

            return true;
        }

        private bool CheckParameter(XElement parameter)
        {
            var attributes = parameter.Attributes();

            XAttribute id, name, description, type, calibration, rangeStart, rangeEnd, unit;
            try
            {
                id = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "id");
                name = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "name");
                description = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "description");
                type = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "type");
                calibration = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "calibration");
                rangeStart = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "rangeStart");
                rangeEnd = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "rangeend");
                unit = attributes.Single(x => x.Name.LocalName.ToLowerInvariant() == "unit");
            }
            catch (InvalidOperationException)
            {
                return false;
            }

            if (!uint.TryParse(id.Value, out uint num))
            {
                return false;
            }
            if (!uint.TryParse(calibration.Value, out num))
            {
                return false;
            }

            string[] types = new string[] { "bitmap", "datetime", "byte", "int", "float", "double" };
            string typeString;
            try
            {
                typeString = types.Single(x => type.Value.ToLowerInvariant() == x);
            }
            catch (InvalidOperationException)
            {
                return false;
            }


            return true;
        }

        /// <summary>
        /// Adds a "Parameter" node with the specified attributes to <see cref="MIBXML"/>
        /// </summary>
        /// <exception cref="InvalidOperationException"/>
        /// <param name="telemetryOrTelecommand">True = telemetry parameter, False = telecommand parameter</param>
        /// <param name="serviceType"></param>
        /// <param name="serviceSubtype"></param>
        /// <param name="id">Number identifier of the parameter among other parameters</param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        /// <param name="calibration"></param>
        /// <param name="rangeStart"></param>
        /// <param name="rangeEnd"></param>
        /// <param name="unit"></param>
        /// <param name="isLittleEndian"></param>
        public void AddParameter(bool telemetryOrTelecommand, byte serviceType, byte serviceSubtype, uint id, string name, string description, string type, uint calibration, uint rangeStart, uint rangeEnd, string unit, bool isLittleEndian = false)
        {
            IEnumerable<XElement> serviceTypes;
            if (telemetryOrTelecommand)
            {
                serviceTypes = MIBXML.Root.Element("telemetry", true).Elements("servicetype", true);
            }
            else
            {
                serviceTypes = MIBXML.Root.Element("telecommands", true).Elements("servicetype", true);
            }

            var serviceSubtypeNode = serviceTypes.Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceType)
                .Elements("servicesubtype", true).Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype);

            if (serviceSubtypeNode.Elements("parameter", true).Any(x => uint.Parse(x.Attribute("id", true).Value) == id))
            {
                throw new InvalidOperationException();
            }

            XElement parameter = new XElement
                (
                    "Parameter",
                    new XAttribute("Id", id.ToString()),
                    new XAttribute("Name", name),
                    new XAttribute("Description", description),
                    new XAttribute("Type", type),
                    new XAttribute("Calibration", calibration.ToString()),
                    new XAttribute("RangeStart", rangeStart.ToString()),
                    new XAttribute("RangeEnd", rangeEnd.ToString()),
                    new XAttribute("Unit", unit),
                    new XAttribute("islittleendian", isLittleEndian.ToString())
                );
            serviceSubtypeNode.Add(parameter);
            MIBXML.Save(this.FileName);
        }

        /// <summary>
        /// Adds a full "ServiceType" node with all subtypes and parameters to <see cref="MIBXML"/>
        /// </summary>
        /// <param name="telemetryOrTelecommand">Indicates whether the service is telemetry or telecommand. True = telemetry service, False = telecommand service.</param>
        /// <param name="serviceType">Full ServiceType object to add to MIB</param>
        public void AddServiceType(bool telemetryOrTelecommand, ServiceTypeFormat serviceType)
        {
            XElement serviceTypeNode = new XElement
                (
                    "ServiceType",
                    new XAttribute("Value", serviceType.Value),
                    new XAttribute("Name", serviceType.Name)
                );

            foreach (ServiceSubtypeFormat serviceSubtype in serviceType.ServiceSubtypes)
            {
                serviceTypeNode.Add(null);
            }
        }

        /*public void AddParameter(bool telemetryOrTelecommand, Parameter parameter)
        {
            IEnumerable<XElement> serviceTypes;
            if (telemetryOrTelecommand)
            {
                serviceTypes = MIBXML.Root.Element("telemetry", true).Elements("servicetype", true);
            }
            else
            {
                serviceTypes = MIBXML.Root.Element("telecommands", true).Elements("servicetype", true);
            }

            var serviceSubtypeNode = serviceTypes.Single(x => byte.Parse(x.Attribute("value").Value) == parameter.ServiceType)
                .Elements("servicesubtype", true).Single(x => byte.Parse(x.Attribute("value").Value) == parameter.ServiceSubtype);

            if (serviceSubtypeNode.Elements("parameter", true).Any(x => uint.Parse(x.Attribute("id").Value) == parameter.Id))
            {
                throw new InvalidOperationException();
            }

            XElement parameterElement = new XElement
                (
                    "parameter",
                    new XAttribute("id", parameter.Id.ToString()),
                    new XAttribute("name", parameter.Name),
                    new XAttribute("description", parameter.Description),
                    new XAttribute("type", type),
                    new XAttribute("calibration", parameter.Calibration.Id),
                    new XAttribute("rangestart", rangeStart.ToString()),
                    new XAttribute("rangeend", rangeEnd.ToString()),
                    new XAttribute("unit", parameter.Unit),
                    new XAttribute("islittleendian", parameter.IsLittleEndian.ToString().ToLowerInvariant())
                );
            serviceSubtypeNode.Add(parameter);
            MIBXML.Save(this.FileName);
        }*/

        #region "Find" Queries

        #region Packets
        public DecipheredPacket[] FindAllPackets(bool telemetryOrTelecommand)
        {
            IEnumerable<XElement> serviceTypes;
            if (telemetryOrTelecommand)
            {
                serviceTypes = MIBXML.Root.Element("telemetry", true).Elements("servicetype", true);
            }
            else
            {
                serviceTypes = MIBXML.Root.Element("telecommands", true).Elements("servicetype", true);
            }

            List<DecipheredPacket> packets = new List<DecipheredPacket>();
            foreach (var serviceType in serviceTypes)
            {
                foreach (var serviceSubtype in serviceType.Elements("servicesubtype", true))
                {
                    List<Parameter> parameters = new List<Parameter>();
                    foreach (var parameter in serviceSubtype.Elements("parameter", true))
                    {
                        parameters.Add(XElementToParameter(parameter, byte.Parse(serviceType.Attribute("value", true).Value), byte.Parse(serviceSubtype.Attribute("value", true).Value), telemetryOrTelecommand));
                    }
                    packets.Add(new DecipheredPacket(0, telemetryOrTelecommand, serviceType.Attribute("name", true).Value + ": " + serviceSubtype.Attribute("name", true).Value,
                        byte.Parse(serviceType.Attribute("value", true).Value), byte.Parse(serviceSubtype.Attribute("value", true).Value), default(DateTime), default(DateTime), parameters.ToArray(), true));
                }
            }
            return packets.ToArray();
        }

        public DecipheredPacket FindCertainPacket(byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            XElement serviceTypeNode = null;
            if (telemetryOrTelecommand)
            {
                serviceTypeNode = MIBXML.Root.Element("telemetry", true)
                    .Elements("servicetype", true).Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceType);
            }
            else
            {
                serviceTypeNode = MIBXML.Root.Element("telecommands", true)
                    .Elements("servicetype", true).Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceType);
            }

            IEnumerable<XElement> parameterNodes = serviceTypeNode.Elements("servicesubtype", true)
                .Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype).Elements("parameter", true);

            if (!parameterNodes.Any())
            {
                throw new InvalidOperationException();
            }

            List<Parameter> parameters = new List<Parameter>();

            foreach (var parameter in serviceTypeNode.Elements("servicesubtype", true)
                .Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype).Elements("parameter", true))
            {
                parameters.Add(XElementToParameter(parameter, serviceType, serviceSubtype, telemetryOrTelecommand));
            }
            DecipheredPacket packet = new DecipheredPacket(0, telemetryOrTelecommand, serviceTypeNode.Attribute("name", true).Value + ": "
                + serviceTypeNode.Elements("servicesubtype", true).Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype).Attribute("name", true).Value,
                serviceType, serviceSubtype, default(DateTime), default(DateTime), parameters.ToArray(), true);

            return packet;
        }
        #endregion

        #region Parameters
        public Parameter[] FindAllParameters(bool telemetryOrTelecommand)
        {
            IEnumerable<XElement> serviceTypes;
            if (telemetryOrTelecommand)
            {
                serviceTypes = MIBXML.Root.Element("telemetry").Elements("servicetype");
            }
            else
            {
                serviceTypes = MIBXML.Root.Element("telecommands").Elements("servicetype");
            }

            List<Parameter> parameters = new List<Parameter>();
            foreach (var serviceType in serviceTypes)
            {
                foreach (var serviceSubtype in serviceType.Elements("servicesubtype"))
                {
                    foreach (var parameter in serviceSubtype.Elements("parameter"))
                    {
                        parameters.Add(XElementToParameter(parameter, byte.Parse(serviceType.Attribute("value", true).Value), byte.Parse(serviceSubtype.Attribute("value", true).Value), telemetryOrTelecommand));
                    }
                }
            }
            return parameters.ToArray();
        }

        /// <summary>
        /// Returns the <see cref="Parameter"/> objects (either telemetry or telecommand), based on the parameter nodes of <see cref="MIBXML"/>, that are contained in the specified Service-Type and Service-Subtype.
        /// Throws an exception if there's no nodes in the specified Service-Type and Service-Subtype, or there are multiple similar Service-Types or Service-Subtypes.
        /// </summary>
        /// <exception cref="InvalidOperationException"/>
        /// <param name="serviceType">The Service-Type of the parameters we request</param>
        /// <param name="serviceSubtype">The Service-Subtype of the parameters we request</param>
        /// <param name="telemetryOrTelecommand">Symbolizes if the method will search for telemetry or for telecommand parameters. True = Telemetry, False = Telecommand.</param>
        /// <returns>An IEnumerable that contains all of the parameters of either telemetry or telecommand, with the specified Service-Type and Service-Subtype</returns>
        public Parameter[] FindParameters(byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            IEnumerable<XElement> parameterNodes;
            if (telemetryOrTelecommand)
            {
                parameterNodes = MIBXML.Root.Element("telemetry")
                    .Elements("servicetype").Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceType)
                    .Elements("servicesubtype").Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype)
                    .Elements("parameter");
            }
            else
            {
                parameterNodes = MIBXML.Root.Element("telecommands")
                    .Elements("servicetype").Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceType)
                    .Elements("servicesubtype").Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype)
                    .Elements("parameter");
            }

            if (!parameterNodes.Any())
            {
                throw new InvalidOperationException();
            }

            List<Parameter> parameters = new List<Parameter>();
            foreach (var parameter in parameterNodes)
            {
                parameters.Add(XElementToParameter(parameter, serviceType, serviceSubtype, telemetryOrTelecommand));
            }

            return parameters.ToArray();
        }

        /// <summary>
        /// Returns the only <see cref="Parameter"/> object (either telemetry or telecommand), based on the parameter node of <see cref="MIBXML"/>, that has the same Service-Type, Service-Subtype and ID.
        /// Throws an exception if there's no such node, or there are multiple similar nodes.
        /// </summary>
        /// <exception cref="InvalidOperationException"/>
        /// <param name="serviceType">The Service-Type of the parameter we request</param>
        /// <param name="serviceSubtype">The Service-Subtype of the parameter we request</param>
        /// <param name="id">The ID of the requested parameter</param>
        /// <param name="telemetryOrTelecommand">Symbolizes if the method will search for telemetry or for telecommand parameter. True = Telemetry, False = Telecommand.</param>
        /// <returns></returns>
        public Parameter FindCertainParameter(byte serviceType, byte serviceSubtype, uint id, bool telemetryOrTelecommand)
        {
            var parameterNode = MIBXML.Root.Element("telemetry")
                .Elements("servicetype").Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceType)
                .Elements("servicesubtype").Single(x => byte.Parse(x.Attribute("value", true).Value) == serviceSubtype)
                .Elements("parameter")
                .Single(x => uint.Parse(x.Attribute("id", true).Value) == id);

            return XElementToParameter(parameterNode, serviceType, serviceSubtype, telemetryOrTelecommand);
        }
        #endregion

        /// <summary>
        /// Finds a calibration in MIB based on gived ID
        /// </summary>
        /// <exception cref="InvalidOperationException"/>
        /// <param name="id">The ID of the requested calibration</param>
        /// <param name="telemetryOrTelecommand">Indicates whether the method will search for telemetry or for telecommand calibration. True = Telemetry, False = Telecommand.</param>
        /// <returns></returns>
        public XElement FindCalibration(uint id, bool telemetryOrTelecommand)
        {
            if (telemetryOrTelecommand)
            {
                return MIBXML.Root.Element("telemetrycals", true).Elements("calibration", true).Single(x => uint.Parse(x.Attribute("id", true).Value) == id);
            }
            else
            {
                return MIBXML.Root.Element("telecommandcals", true).Elements("calibration", true).Single(x => uint.Parse(x.Attribute("id", true).Value) == id);
            }
        }

        /// <summary>
        /// Finds full MIB data in format for GSC-Controller to edit
        /// </summary>
        /// <param name="telemetryOrTelecommand">True = Telemetry, False = Telecommand</param>
        /// <returns></returns>
        public ServiceTypeFormat[] FindFullMissionFormat(bool telemetryOrTelecommand)
        {
            IEnumerable<XElement> serviceTypes;
            if (telemetryOrTelecommand)
            {
                serviceTypes = MIBXML.Root.Element("telemetry", true).Elements("servicetype", true);
            }
            else
            {
                serviceTypes = MIBXML.Root.Element("telecommands", true).Elements("servicetype", true);
            }

            List<ServiceTypeFormat> serviceModels = new List<ServiceTypeFormat>();
            foreach (var serviceType in serviceTypes)
            {
                List<ServiceSubtypeFormat> subserviceModels = new List<ServiceSubtypeFormat>();
                foreach (var serviceSubtype in serviceType.Elements("servicesubtype", true))
                {
                    List<ParameterFormat> parameterModels = new List<ParameterFormat>();
                    foreach (var parameter in serviceSubtype.Elements("parameter", true))
                    {
                        var newParameterModel = XElementToParameterModel(parameter);
                        parameterModels.Add(newParameterModel);
                    }
                    var newSubserviceModel = new ServiceSubtypeFormat(serviceSubtype.Attribute("name", true).Value, byte.Parse(serviceSubtype.Attribute("value", true).Value), parameterModels);
                    subserviceModels.Add(newSubserviceModel);
                }
                var newServiceModel = new ServiceTypeFormat(telemetryOrTelecommand, serviceType.Attribute("name", true).Value, byte.Parse(serviceType.Attribute("value", true).Value), subserviceModels);
                serviceModels.Add(newServiceModel);
            }
            return serviceModels.ToArray();
        }
        #endregion

        #region XElement Conversion
        /// <summary>
        /// Returns a <see cref="Parameter"/> object based on the specified "Parameter" node (either telemetry or telecommand).
        /// </summary>
        /// <param name="parameterNode">The "Parameter" node that we want to be extracted</param>
        /// <param name="serviceType">The Service-Type of the parameter we extract</param>
        /// <param name="serviceSubtype">The Service-Subtype of the parameter we extract</param>
        /// <param name="telemetryOrTelecommand">Symbolizes if the method extracts a telemetry or a telecommand parameter. True = Telemetry, False = Telecommand.</param>
        /// <returns></returns>
        public Parameter XElementToParameter(XElement parameterNode, byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            bool isThereCalibration = parameterNode.Attribute("calibration", true).Value != null;
            bool isThereLittle = parameterNode.Attribute("islittleendian", true).Value != null;
            XElement calibration = null;
            bool isLittleEndian = false;

            if (isThereCalibration)
            {
                calibration = FindCalibration(uint.Parse(parameterNode.Attribute("calibration", true).Value), telemetryOrTelecommand);
            }
            if (isThereLittle)
            {
                isLittleEndian = false;
                if (parameterNode.Attribute("islittleendian", true).Value == "true")
                {
                    isLittleEndian = true;
                }
                else if (parameterNode.Attribute("islittleendian", true).Value == "false")
                {
                    isLittleEndian = false;
                }
                else
                {
                    throw new FormatException();
                }
            }

            string type = parameterNode.Attribute("type", true).Value.ToLowerInvariant();
            if (type == "bitmap")
            {
                return XElementToBitmapParameter(parameterNode, serviceType, serviceSubtype, telemetryOrTelecommand);
            }
            else if (type == "byte")
            {
                return new ByteParameter
                (
                    uint.Parse(parameterNode.Attribute("id", true).Value),
                    parameterNode.Attribute("name", true).Value,
                    parameterNode.Attribute("description", true).Value,
                    serviceType,
                    serviceSubtype,
                    byte.Parse(parameterNode.Attribute("rangestart", true).Value),
                    byte.Parse(parameterNode.Attribute("rangeend", true).Value),
                    calibration,
                    parameterNode.Attribute("unit", true).Value
                );
            }
            else if (type == "datetime")
            {
                return new DateTimeParameter
                (
                    uint.Parse(parameterNode.Attribute("id", true).Value),
                    parameterNode.Attribute("name", true).Value,
                    parameterNode.Attribute("description", true).Value,
                    serviceType,
                    serviceSubtype,
                    DateTime.Parse(parameterNode.Attribute("rangestart", true).Value),
                    DateTime.Parse(parameterNode.Attribute("rangeend", true).Value),
                    calibration,
                    parameterNode.Attribute("unit", true).Value,
                    isLittleEndian
                );
            }
            else if (type == "double")
            {
                return new DoubleParameter
                (
                    uint.Parse(parameterNode.Attribute("id", true).Value),
                    parameterNode.Attribute("name", true).Value,
                    parameterNode.Attribute("description", true).Value,
                    serviceType,
                    serviceSubtype,
                    double.Parse(parameterNode.Attribute("rangestart", true).Value),
                    double.Parse(parameterNode.Attribute("rangeend", true).Value),
                    calibration,
                    parameterNode.Attribute("unit", true).Value,
                    isLittleEndian
                );
            }
            else if (type == "float")
            {
                return new FloatParameter
                (
                    uint.Parse(parameterNode.Attribute("id", true).Value),
                    parameterNode.Attribute("name", true).Value,
                    parameterNode.Attribute("description", true).Value,
                    serviceType,
                    serviceSubtype,
                    float.Parse(parameterNode.Attribute("rangestart", true).Value),
                    float.Parse(parameterNode.Attribute("rangeend", true).Value),
                    calibration,
                    parameterNode.Attribute("unit", true).Value,
                    isLittleEndian
                );
            }
            else if (type == "int")
            {
                return new IntParameter
                (
                    uint.Parse(parameterNode.Attribute("id", true).Value),
                    parameterNode.Attribute("name", true).Value,
                    parameterNode.Attribute("description", true).Value,
                    serviceType,
                    serviceSubtype,
                    int.Parse(parameterNode.Attribute("rangestart", true).Value),
                    int.Parse(parameterNode.Attribute("rangeend", true).Value),
                    calibration,
                    parameterNode.Attribute("unit", true).Value,
                    isLittleEndian
                );
            }
            else throw new InvalidOperationException();
        }

        public BitmapParameter XElementToBitmapParameter(XElement parameterNode, byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            var fieldNodes = parameterNode.Elements("bitfield").ToArray();
            if (fieldNodes.Select(x => int.Parse(x.Attribute("size", true).Value)).Sum() / 8 > sizeof(byte))
            {
                throw new InvalidOperationException();
            }
            var fields = new BitmapParameter.Bitfield[fieldNodes.Length];
            for (int idx = 0; idx < fields.Length; idx++)
            {
                bool isNull = true;
                if (fieldNodes[idx].Attribute("isnull", true).Value == "true")
                {
                    isNull = true;
                }
                else if (fieldNodes[idx].Attribute("isnull", true).Value == "false")
                {
                    isNull = false;
                }
                else
                {
                    throw new FormatException();
                }

                if (isNull)
                {
                    fields[idx] = new BitmapParameter.Bitfield(int.Parse(fieldNodes[idx].Attribute("size", true).Value), isNull);
                }
                else
                {
                    XElement calibration = FindCalibration(uint.Parse(fieldNodes[idx].Attribute("calibration", true).Value), telemetryOrTelecommand);
                    fields[idx] = new BitmapParameter.Bitfield
                    (
                        int.Parse(fieldNodes[idx].Attribute("size", true).Value),
                        isNull,
                        calibration,
                        fieldNodes[idx].Attribute("name", true).Value,
                        fieldNodes[idx].Attribute("description", true).Value
                    );
                }
            }

            return new BitmapParameter
                    (
                        uint.Parse(parameterNode.Attribute("id", true).Value),
                        parameterNode.Attribute("name", true).Value,
                        parameterNode.Attribute("description", true).Value,
                        serviceType,
                        serviceSubtype,
                        fields
                    );
        }

        public ParameterFormat XElementToParameterModel(XElement parameterNode)
        {
            return new ParameterFormat
            (
                uint.Parse(parameterNode.Attribute("id", true).Value),
                parameterNode.Attribute("name", true).Value,
                parameterNode.Attribute("description", true).Value,
                false,
                parameterNode.Attribute("unit", true).Value
            );
        }
        #endregion

        /// <summary>
        /// Edits the MIB - swaps every attribute of format nodes with values in given format
        /// </summary>
        /// <param name="telemetryOrTelecommand">True = Telemetry, False = Telecommand</param>
        /// <param name="serviceTypeModels">Full services format with updated attribute values</param>
        public void UpdateFullMissionFormat(bool telemetryOrTelecommand, ServiceTypeFormat[] serviceTypeModels)
        {
            IEnumerable<XElement> serviceTypes;
            if (telemetryOrTelecommand)
            {
                serviceTypes = MIBXML.Root.Element("telemetry", true).Elements("servicetype", true);
            }
            else
            {
                serviceTypes = MIBXML.Root.Element("telecommands", true).Elements("servicetype", true);
            }

            foreach (var serviceTypeModel in serviceTypeModels)
            {
                XElement editingServiceType;
                try
                {
                    editingServiceType = serviceTypes.Single(x => x.Attribute("value", true).Value == serviceTypeModel.PreviousValue.ToString());
                }
                catch (InvalidOperationException)
                {
                    AddServiceType(telemetryOrTelecommand, serviceTypeModel);
                    return;
                }

                editingServiceType.Attribute("name", true).Value = serviceTypeModel.Name;
                editingServiceType.Attribute("value", true).Value = serviceTypeModel.Value.ToString();

                var serviceSubtypes = editingServiceType.Elements("servicesubtype", true);
                foreach (ServiceSubtypeFormat serviceSubtypeModel in serviceTypeModel.ServiceSubtypes)
                {
                    XElement editingServiceSubtype;
                    try
                    {
                        editingServiceSubtype = serviceSubtypes.Single(x => x.Attribute("value", true).Value == serviceSubtypeModel.PreviousValue.ToString());
                    }
                    catch (InvalidOperationException)
                    {
                        return;
                    }

                    editingServiceSubtype.Attribute("name", true).Value = serviceSubtypeModel.Name;
                    editingServiceSubtype.Attribute("value", true).Value = serviceSubtypeModel.Value.ToString();

                    var parameters = editingServiceSubtype.Elements("servicesubtype", true);
                    foreach (ParameterFormat parameterModel in serviceSubtypeModel.Parameters)
                    {
                        XElement editingParameter;
                        try
                        {
                            editingParameter = editingServiceSubtype.Elements("parameter", true)
                                .Single();
                        }
                        catch (InvalidOperationException)
                        {
                            return;
                        }
                    }
                }

                serviceTypeModel.UpdatePreviousIDsRecursively();
            }
            MIBXML.Save(this.FileName);
        }
    }
}
