﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSL.GSC.Base.Data.SPL;
using HSL.GSC.Base.Data.MissionInfromation;
using HSL.GSC.CommunicationFormat.Packets;

namespace HSL.GSC.Base.Data.Storage
{
    public abstract class StorageController
    {
        /*public void AddPacket(Packet packet)
        {
            if (packet is TelemetryPacket)
            {
                AddTelemetryPacket(packet as TelemetryPacket);
            }
            else if (packet is TelecommandPacket)
            {
                AddTelecommandPacket(packet as TelecommandPacket);
            }
            else
            {
                throw new ArgumentException("The packet is of unknown type");
            }
        }*/

        public abstract IStorageElement AddTelemetryPacket(TelemetryPacket packet);

        public abstract IStorageElement AddTelecommandPacket(TelecommandPacket packet);

        #region Packets Decipher
        private static DecipheredPacket DecipherCompletion(DecipheredPacket packet, byte[] data)
        {
            Queue<byte> remainedPacketData = new Queue<byte>(data);
            foreach (var parameter in packet.Parameters)
            {
                if (remainedPacketData.Count < parameter.Size) break;
                byte[] dataToPack = new byte[parameter.Size];
                for (int idx = 0; idx < dataToPack.Length; idx++)
                {
                    dataToPack[idx] = remainedPacketData.Dequeue();
                }
                parameter.RawData = dataToPack;
            }
            return packet;
        }

        public static DecipheredPacket[] Decipher(MissionInfromationController controller, IEnumerable<DatabaseTelemetry> telemetries)
        {
            List<DecipheredPacket> toReturn = new List<DecipheredPacket>();
            foreach (var telemetry in telemetries)
            {
                DecipheredPacket packet = controller.FindCertainPacket(telemetry.ServiceType, telemetry.ServiceSubtype, true);
                packet.Id = telemetry.Id;
                packet.GroundDateTime = telemetry.GroundDateTime;
                packet.SatDateTime = telemetry.SatDateTime;
                toReturn.Add(DecipherCompletion(packet, telemetry.Data.ToArray()));
            }
            return toReturn.ToArray();
        }

        public static DecipheredPacket[] Decipher(MissionInfromationController controller, IEnumerable<DatabaseTelecommand> telecommands)
        {
            List<DecipheredPacket> toReturn = new List<DecipheredPacket>();
            foreach (var telecommand in telecommands)
            {
                DecipheredPacket packet = controller.FindCertainPacket(telecommand.ServiceType, telecommand.ServiceSubtype, true);
                packet.Id = telecommand.Id;
                packet.GroundDateTime = telecommand.GroundDateTime;
                toReturn.Add(DecipherCompletion(packet, telecommand.Data.ToArray()));
            }
            return toReturn.ToArray();
        }

        public static DecipheredPacket[] Decipher(MissionInfromationController controller, IEnumerable<TelemetryPacket> telemetries)
        {
            List<DecipheredPacket> toReturn = new List<DecipheredPacket>();
            foreach (var telemetry in telemetries)
            {
                DecipheredPacket packet = controller.FindCertainPacket(telemetry.ServiceType, telemetry.ServiceSubtype, true);
                packet.GroundDateTime = DateTime.Now;
                packet.SatDateTime = telemetry.Time;
                toReturn.Add(DecipherCompletion(packet, telemetry.Data));
            }
            return toReturn.ToArray();
        }

        public static DecipheredPacket[] Decipher(MissionInfromationController controller, IEnumerable<TelecommandPacket> telecommands)
        {
            List<DecipheredPacket> toReturn = new List<DecipheredPacket>();
            foreach (var telecommand in telecommands)
            {
                DecipheredPacket packet = controller.FindCertainPacket(telecommand.ServiceType, telecommand.ServiceSubtype, true);
                packet.GroundDateTime = telecommand.Time;
                toReturn.Add(DecipherCompletion(packet, telecommand.Data));
            }
            return toReturn.ToArray();
        }

        public static DecipheredPacket Decipher(DecipheredPacket packet, byte[] data)
        {
            DecipheredPacket copy = new DecipheredPacket(packet);
            return DecipherCompletion(copy, data);
        }

        public static DecipheredPacket Decipher(DecipheredPacket packet, uint id, DateTime groundDateTime, DateTime satDateTime, byte[] data)
        {
            DecipheredPacket copy = new DecipheredPacket(packet);
            copy.Id = id;
            copy.GroundDateTime = groundDateTime;
            copy.SatDateTime = satDateTime;
            return DecipherCompletion(copy, data);
        }
        #endregion
    }
}
