﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.Base.Data.Storage
{
    public interface IStorageElement
    {
        long Id { get; set; }
        DateTime GroundDateTime { get; set; }
        byte ServiceType { get; set; }
        byte ServiceSubtype { get; set; }
        System.Data.Linq.Binary Data { get; set; }
    }
}
