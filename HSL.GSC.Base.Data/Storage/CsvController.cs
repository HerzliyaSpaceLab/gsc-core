﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using HSL.GSC.Base.Data.SPL;
using HSL.GSC.Base.Data.MissionInfromation;
using HSL.GSC.CommunicationFormat.Packets;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using HSL.GSC.CommunicationFormat.MissionFormat;

namespace HSL.GSC.Base.Data.Storage
{
    public class CsvController : StorageController
    {
        private string TelemetryDirectory { get; set; }
        private MissionInfromationController MissionController { get; set; }
        public uint TelemetryId { get; private set; }
        public uint TelecommandId { get; private set; }

        public CsvController(string directory, MissionInfromationController controller)
        {
            if (!Directory.Exists(directory))
            {
                throw new ArgumentException();
            }
            this.TelemetryId = 1;
            this.TelecommandId = 1000;
            this.TelemetryDirectory = directory;
            this.MissionController = controller;
        }

        public override IStorageElement AddTelemetryPacket(TelemetryPacket packet)
        {
            DateTime creation = default(DateTime);
            bool success = false;
            for (int idx = 0; idx < 3 && !success; idx++)
            {
                creation = DateTime.Now;

                string newFileName = Path.Combine(TelemetryDirectory, "TM_" + creation.Year + "-" + creation.Month + "-" + creation.Day + "_" +
                    creation.Hour + "-" + creation.Minute + "-" + creation.Second + "_ST-" +
                    packet.ServiceType + "_SST-" + packet.ServiceSubtype + ".csv");

                if (File.Exists(newFileName))
                {
                    Thread.Sleep(1000);
                }
                else
                {
                    var writer = new StreamWriter(File.Create(newFileName));
                    DecipheredPacket[] result = Decipher(MissionController, new TelemetryPacket[] { packet });
                    EncodeCsv(writer, result.First());
                    writer.Close();
                    success = true;
                }
            }

            CsvElement toReturn = null;
            if (success)
            {
                toReturn = CsvElement.FromTelemetry(packet, TelemetryId, creation);
                TelemetryId++;
            }
            return toReturn;
        }

        public override IStorageElement AddTelecommandPacket(TelecommandPacket packet)
        {
            DateTime creation = default(DateTime);
            bool success = false;
            for (int idx = 0; idx < 3 && !success; idx++)
            {
                creation = DateTime.Now;

                string newFileName = Path.Combine(TelemetryDirectory, "TC_" + creation.Year + "-" + creation.Month + "-" + creation.Day + "_" +
                    creation.Hour + "-" + creation.Minute + "-" + creation.Second + "_ST-" +
                    packet.ServiceType + "_SST-" + packet.ServiceSubtype + ".csv");

                if (File.Exists(newFileName))
                {
                    Thread.Sleep(1000);
                }
                else
                {
                    var writer = new StreamWriter(File.Create(newFileName));
                    DecipheredPacket[] result = Decipher(MissionController, new TelecommandPacket[] { packet });
                    EncodeCsv(writer, result.First());
                    writer.Close();
                    success = true;
                }
            }

            CsvElement toReturn = null;
            if (success)
            {
                toReturn = CsvElement.FromTelecommand(packet, TelecommandId, creation);
                TelecommandId++;
            }
            return toReturn;
        }

        private void EncodeCsv(StreamWriter writer, DecipheredPacket packet)
        {
            writer.WriteLine("Parameter Id,Parameter Name,Raw Data,Value,Unit");
            foreach (var parameter in packet.Parameters)
            {
                writer.WriteLine(parameter.Id + "," + parameter.Name + "," + BitConverter.ToString(parameter.RawData) + "," + parameter.Data + ","
                    + (parameter is IUnitDefinable ? (parameter as IUnitDefinable).Unit : "none"));
            }
        }
    }

    class CsvElement : IStorageElement
    {
        public long Id { get; set; }
        public DateTime GroundDateTime { get; set; }
        public byte ServiceType { get; set; }
        public byte ServiceSubtype { get; set; }
        public System.Data.Linq.Binary Data { get; set; }

        public static CsvElement FromTelemetry(TelemetryPacket telemetry, long id, DateTime groundTime)
        {
            return new CsvElement
            {
                Id = id,
                GroundDateTime = groundTime,
                ServiceType = telemetry.ServiceType,
                ServiceSubtype = telemetry.ServiceSubtype,
                Data = new System.Data.Linq.Binary((byte[])telemetry.Data.Clone())
            };
        }
        public static CsvElement FromTelecommand(TelecommandPacket telecommand, long id, DateTime groundTime)
        {
            return new CsvElement
            {
                Id = id,
                GroundDateTime = groundTime,
                ServiceType = telecommand.ServiceType,
                ServiceSubtype = telecommand.ServiceSubtype,
                Data = new System.Data.Linq.Binary((byte[])telecommand.Data.Clone())
            };
        }
    }
}
