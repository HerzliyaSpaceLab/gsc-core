﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HSL.GSC.Base.Data.Storage
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Database")]
	public partial class LINQtoSQLDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertDatabaseTelemetry(DatabaseTelemetry instance);
    partial void UpdateDatabaseTelemetry(DatabaseTelemetry instance);
    partial void DeleteDatabaseTelemetry(DatabaseTelemetry instance);
    partial void InsertDatabaseScientificData(DatabaseScientificData instance);
    partial void UpdateDatabaseScientificData(DatabaseScientificData instance);
    partial void DeleteDatabaseScientificData(DatabaseScientificData instance);
    partial void InsertDatabaseTelecommand(DatabaseTelecommand instance);
    partial void UpdateDatabaseTelecommand(DatabaseTelecommand instance);
    partial void DeleteDatabaseTelecommand(DatabaseTelecommand instance);
    #endregion
		
		public LINQtoSQLDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public LINQtoSQLDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public LINQtoSQLDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public LINQtoSQLDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<DatabaseTelemetry> DatabaseTelemetries
		{
			get
			{
				return this.GetTable<DatabaseTelemetry>();
			}
		}
		
		public System.Data.Linq.Table<DatabaseScientificData> DatabaseScientificDatas
		{
			get
			{
				return this.GetTable<DatabaseScientificData>();
			}
		}
		
		public System.Data.Linq.Table<DatabaseTelecommand> DatabaseTelecommands
		{
			get
			{
				return this.GetTable<DatabaseTelecommand>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Telemetry")]
	public partial class DatabaseTelemetry : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _Id;
		
		private System.DateTime _GroundDateTime;
		
		private System.DateTime _SatDateTime;
		
		private byte _ServiceType;
		
		private byte _ServiceSubtype;
		
		private System.Data.Linq.Binary _Data;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(long value);
    partial void OnIdChanged();
    partial void OnGroundDateTimeChanging(System.DateTime value);
    partial void OnGroundDateTimeChanged();
    partial void OnSatDateTimeChanging(System.DateTime value);
    partial void OnSatDateTimeChanged();
    partial void OnServiceTypeChanging(byte value);
    partial void OnServiceTypeChanged();
    partial void OnServiceSubtypeChanging(byte value);
    partial void OnServiceSubtypeChanged();
    partial void OnDataChanging(System.Data.Linq.Binary value);
    partial void OnDataChanged();
    #endregion
		
		public DatabaseTelemetry()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true, UpdateCheck=UpdateCheck.Never)]
		public long Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_GroundDateTime", DbType="DateTime NOT NULL", UpdateCheck=UpdateCheck.Never)]
		public System.DateTime GroundDateTime
		{
			get
			{
				return this._GroundDateTime;
			}
			set
			{
				if ((this._GroundDateTime != value))
				{
					this.OnGroundDateTimeChanging(value);
					this.SendPropertyChanging();
					this._GroundDateTime = value;
					this.SendPropertyChanged("GroundDateTime");
					this.OnGroundDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SatDateTime", DbType="DateTime NOT NULL", UpdateCheck=UpdateCheck.Never)]
		public System.DateTime SatDateTime
		{
			get
			{
				return this._SatDateTime;
			}
			set
			{
				if ((this._SatDateTime != value))
				{
					this.OnSatDateTimeChanging(value);
					this.SendPropertyChanging();
					this._SatDateTime = value;
					this.SendPropertyChanged("SatDateTime");
					this.OnSatDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ServiceType", DbType="TinyInt NOT NULL", UpdateCheck=UpdateCheck.Never)]
		public byte ServiceType
		{
			get
			{
				return this._ServiceType;
			}
			set
			{
				if ((this._ServiceType != value))
				{
					this.OnServiceTypeChanging(value);
					this.SendPropertyChanging();
					this._ServiceType = value;
					this.SendPropertyChanged("ServiceType");
					this.OnServiceTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ServiceSubtype", DbType="TinyInt NOT NULL", UpdateCheck=UpdateCheck.Never)]
		public byte ServiceSubtype
		{
			get
			{
				return this._ServiceSubtype;
			}
			set
			{
				if ((this._ServiceSubtype != value))
				{
					this.OnServiceSubtypeChanging(value);
					this.SendPropertyChanging();
					this._ServiceSubtype = value;
					this.SendPropertyChanged("ServiceSubtype");
					this.OnServiceSubtypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Data", DbType="VarBinary(MAX) NOT NULL", CanBeNull=false, UpdateCheck=UpdateCheck.Never)]
		public System.Data.Linq.Binary Data
		{
			get
			{
				return this._Data;
			}
			set
			{
				if ((this._Data != value))
				{
					this.OnDataChanging(value);
					this.SendPropertyChanging();
					this._Data = value;
					this.SendPropertyChanged("Data");
					this.OnDataChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Payload")]
	public partial class DatabaseScientificData : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _SatId;
		
		private System.DateTime _GroundDateTime;
		
		private System.DateTime _SatDateTime;
		
		private byte _ServiceType;
		
		private byte _ServiceSubtype;
		
		private System.Data.Linq.Binary _Data;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnSatIdChanging(long value);
    partial void OnSatIdChanged();
    partial void OnGroundDateTimeChanging(System.DateTime value);
    partial void OnGroundDateTimeChanged();
    partial void OnSatDateTimeChanging(System.DateTime value);
    partial void OnSatDateTimeChanged();
    partial void OnServiceTypeChanging(byte value);
    partial void OnServiceTypeChanged();
    partial void OnServiceSubtypeChanging(byte value);
    partial void OnServiceSubtypeChanged();
    partial void OnDataChanging(System.Data.Linq.Binary value);
    partial void OnDataChanged();
    #endregion
		
		public DatabaseScientificData()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SatId", DbType="BigInt NOT NULL", IsPrimaryKey=true)]
		public long SatId
		{
			get
			{
				return this._SatId;
			}
			set
			{
				if ((this._SatId != value))
				{
					this.OnSatIdChanging(value);
					this.SendPropertyChanging();
					this._SatId = value;
					this.SendPropertyChanged("SatId");
					this.OnSatIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_GroundDateTime", DbType="DateTime NOT NULL")]
		public System.DateTime GroundDateTime
		{
			get
			{
				return this._GroundDateTime;
			}
			set
			{
				if ((this._GroundDateTime != value))
				{
					this.OnGroundDateTimeChanging(value);
					this.SendPropertyChanging();
					this._GroundDateTime = value;
					this.SendPropertyChanged("GroundDateTime");
					this.OnGroundDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SatDateTime", DbType="DateTime NOT NULL")]
		public System.DateTime SatDateTime
		{
			get
			{
				return this._SatDateTime;
			}
			set
			{
				if ((this._SatDateTime != value))
				{
					this.OnSatDateTimeChanging(value);
					this.SendPropertyChanging();
					this._SatDateTime = value;
					this.SendPropertyChanged("SatDateTime");
					this.OnSatDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ServiceType", DbType="TinyInt NOT NULL")]
		public byte ServiceType
		{
			get
			{
				return this._ServiceType;
			}
			set
			{
				if ((this._ServiceType != value))
				{
					this.OnServiceTypeChanging(value);
					this.SendPropertyChanging();
					this._ServiceType = value;
					this.SendPropertyChanged("ServiceType");
					this.OnServiceTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ServiceSubtype", DbType="TinyInt NOT NULL")]
		public byte ServiceSubtype
		{
			get
			{
				return this._ServiceSubtype;
			}
			set
			{
				if ((this._ServiceSubtype != value))
				{
					this.OnServiceSubtypeChanging(value);
					this.SendPropertyChanging();
					this._ServiceSubtype = value;
					this.SendPropertyChanged("ServiceSubtype");
					this.OnServiceSubtypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Data", DbType="VarBinary(MAX)", CanBeNull=true, UpdateCheck=UpdateCheck.Never)]
		public System.Data.Linq.Binary Data
		{
			get
			{
				return this._Data;
			}
			set
			{
				if ((this._Data != value))
				{
					this.OnDataChanging(value);
					this.SendPropertyChanging();
					this._Data = value;
					this.SendPropertyChanged("Data");
					this.OnDataChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Telecommand")]
	public partial class DatabaseTelecommand : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private long _Id;
		
		private System.DateTime _GroundDateTime;
		
		private byte _ServiceType;
		
		private byte _ServiceSubtype;
		
		private System.Data.Linq.Binary _Data;
		
		private bool _Received;
		
		private bool _Completed;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(long value);
    partial void OnIdChanged();
    partial void OnGroundDateTimeChanging(System.DateTime value);
    partial void OnGroundDateTimeChanged();
    partial void OnServiceTypeChanging(byte value);
    partial void OnServiceTypeChanged();
    partial void OnServiceSubtypeChanging(byte value);
    partial void OnServiceSubtypeChanged();
    partial void OnDataChanging(System.Data.Linq.Binary value);
    partial void OnDataChanged();
    partial void OnReceivedChanging(bool value);
    partial void OnReceivedChanged();
    partial void OnCompletedChanging(bool value);
    partial void OnCompletedChanged();
    #endregion
		
		public DatabaseTelecommand()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="BigInt NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public long Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_GroundDateTime", DbType="DateTime NOT NULL")]
		public System.DateTime GroundDateTime
		{
			get
			{
				return this._GroundDateTime;
			}
			set
			{
				if ((this._GroundDateTime != value))
				{
					this.OnGroundDateTimeChanging(value);
					this.SendPropertyChanging();
					this._GroundDateTime = value;
					this.SendPropertyChanged("GroundDateTime");
					this.OnGroundDateTimeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ServiceType", DbType="TinyInt NOT NULL")]
		public byte ServiceType
		{
			get
			{
				return this._ServiceType;
			}
			set
			{
				if ((this._ServiceType != value))
				{
					this.OnServiceTypeChanging(value);
					this.SendPropertyChanging();
					this._ServiceType = value;
					this.SendPropertyChanged("ServiceType");
					this.OnServiceTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ServiceSubtype", DbType="TinyInt NOT NULL")]
		public byte ServiceSubtype
		{
			get
			{
				return this._ServiceSubtype;
			}
			set
			{
				if ((this._ServiceSubtype != value))
				{
					this.OnServiceSubtypeChanging(value);
					this.SendPropertyChanging();
					this._ServiceSubtype = value;
					this.SendPropertyChanged("ServiceSubtype");
					this.OnServiceSubtypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Data", DbType="VarBinary(MAX) NOT NULL", CanBeNull=false, UpdateCheck=UpdateCheck.Never)]
		public System.Data.Linq.Binary Data
		{
			get
			{
				return this._Data;
			}
			set
			{
				if ((this._Data != value))
				{
					this.OnDataChanging(value);
					this.SendPropertyChanging();
					this._Data = value;
					this.SendPropertyChanged("Data");
					this.OnDataChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Received", DbType="Bit NOT NULL")]
		public bool Received
		{
			get
			{
				return this._Received;
			}
			set
			{
				if ((this._Received != value))
				{
					this.OnReceivedChanging(value);
					this.SendPropertyChanging();
					this._Received = value;
					this.SendPropertyChanged("Received");
					this.OnReceivedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Completed", DbType="Bit NOT NULL")]
		public bool Completed
		{
			get
			{
				return this._Completed;
			}
			set
			{
				if ((this._Completed != value))
				{
					this.OnCompletedChanging(value);
					this.SendPropertyChanging();
					this._Completed = value;
					this.SendPropertyChanged("Completed");
					this.OnCompletedChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
