using HSL.GSC.CommunicationFormat.Packets;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using HSL.GSC.Base.Data.SPL;

namespace HSL.GSC.Base.Data.Storage
{
    partial class DatabaseTelemetry : IStorageElement
    {
        public DatabaseTelemetry(DatabaseTelemetry telemetry)
        {
            this.Id = telemetry.Id;
            this.GroundDateTime = telemetry.GroundDateTime;
            this.SatDateTime = telemetry.SatDateTime;
            this.ServiceType = telemetry.ServiceType;
            this.ServiceSubtype = telemetry.ServiceSubtype;
            this.Data = new System.Data.Linq.Binary((byte[])telemetry.Data.ToArray().Clone());
        }

        public static explicit operator StandardTelemetry(DatabaseTelemetry telemetry)
        {
            return new StandardTelemetry(telemetry.Id, telemetry.GroundDateTime, telemetry.SatDateTime, telemetry.ServiceType, telemetry.ServiceSubtype, (byte[])telemetry.Data.ToArray().Clone());
        }
    }
    partial class DatabaseTelecommand : IStorageElement
    {
        public DatabaseTelecommand(DatabaseTelecommand telecommand)
        {
            this.Id = telecommand.Id;
            this.GroundDateTime = telecommand.GroundDateTime;
            this.ServiceType = telecommand.ServiceType;
            this.ServiceSubtype = telecommand.ServiceSubtype;
            this.Data = new System.Data.Linq.Binary((byte[])telecommand.Data.ToArray().Clone());
            this.Received = telecommand.Received;
            this.Completed = telecommand.Completed;
        }

        public static explicit operator StandardTelecommand(DatabaseTelecommand telecommand)
        {
            return new StandardTelecommand(telecommand.Id, telecommand.GroundDateTime, telecommand.ServiceType, telecommand.ServiceSubtype, (byte[])telecommand.Data.ToArray().Clone());
        }

        /*public static explicit operator TelecommandPacket(DatabaseTelecommand telecommand)
        {
            return new TelecommandPacket((uint)telecommand.Id, telecommand.ServiceType, telecommand.ServiceSubtype, telecommand.GroundDateTime, (byte[])telecommand.Data.ToArray().Clone());
        }*/
    }
}