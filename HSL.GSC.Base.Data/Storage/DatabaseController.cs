﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSL.GSC.Base.Data.SPL;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using HSL.GSC.Base.Data.MissionInfromation;
using HSL.GSC.CommunicationFormat.Packets;
using System.Diagnostics;
using HSL.GSC.API;
using HSL.GSC.CommunicationFormat.MissionFormat;

namespace HSL.GSC.Base.Data.Storage
{
    public class DatabaseController : StorageController
    {
        private readonly string connectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database\Database.mdf;Integrated Security = True; Connect Timeout = 30";
        private object locker = new object();

        public DatabaseController(string databasePath)
        {
            if (!System.IO.File.Exists(databasePath))
            {
                throw new ArgumentException("Database file in the path does not exist");
            }
            this.connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename={databasePath}; Integrated Security=True; Connect Timeout = 30";
            try
            {
                var db = new LINQtoSQLDataContext(this.connectionString);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to connect to database with provided file path", ex);
            }
        }

        #region Add
        public override IStorageElement AddTelemetryPacket(TelemetryPacket packet)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    DatabaseTelemetry toAdd = new DatabaseTelemetry
                    {
                        Data = packet.Data,
                        GroundDateTime = DateTime.Now,
                        SatDateTime = packet.Time,
                        ServiceSubtype = packet.ServiceSubtype,
                        ServiceType = packet.ServiceType
                    };
                    db.DatabaseTelemetries.InsertOnSubmit(toAdd);
                    db.SubmitChanges();
                    return new DatabaseTelemetry(toAdd);
                }
            }
        }

        public override IStorageElement AddTelecommandPacket(TelecommandPacket packet)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    DatabaseTelecommand toAdd = new DatabaseTelecommand
                    {
                        Data = packet.Data,
                        GroundDateTime = DateTime.Now,
                        ServiceSubtype = packet.ServiceSubtype,
                        ServiceType = packet.ServiceType
                    };
                    db.DatabaseTelecommands.InsertOnSubmit(toAdd);
                    db.SubmitChanges();
                    return new DatabaseTelecommand(toAdd);
                }
            }
        }

        public uint AddPacket(DecipheredPacket packet)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    if (packet.TelemetryOrTelecommand)
                    {
                        DatabaseTelemetry toAdd = new DatabaseTelemetry
                        {
                            Data = packet.TotalRawData,
                            GroundDateTime = DateTime.Now,
                            SatDateTime = packet.SatDateTime,
                            ServiceSubtype = packet.ServiceSubtype,
                            ServiceType = packet.ServiceType
                        };
                        db.DatabaseTelemetries.InsertOnSubmit(toAdd);
                        db.SubmitChanges();
                        return (uint)toAdd.Id;
                    }
                    else
                    {
                        DatabaseTelecommand toAdd = new DatabaseTelecommand
                        {
                            Data = packet.TotalRawData,
                            GroundDateTime = DateTime.Now,
                            ServiceSubtype = packet.ServiceSubtype,
                            ServiceType = packet.ServiceType
                        };
                        db.DatabaseTelecommands.InsertOnSubmit(toAdd);
                        db.SubmitChanges();
                        return (uint)toAdd.Id;
                    }
                }
            }
        }
        #endregion

        public IDatabaseData AddScientificPacket(IDatabaseData requiredDataInformation)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    DatabaseScientificData toAdd = new DatabaseScientificData
                    {
                        SatId = requiredDataInformation.PayloadId,
                        Data = null,
                        GroundDateTime = DateTime.Now,
                        SatDateTime = requiredDataInformation.Time,
                        ServiceSubtype = requiredDataInformation.ServiceSubtype,
                        ServiceType = requiredDataInformation.ServiceType
                    };
                    db.DatabaseScientificDatas.InsertOnSubmit(toAdd);
                    db.SubmitChanges();
                    return new PayloadData(requiredDataInformation, null);
                }
            }
        }

        public IDatabaseData FindScientificPacket(IDatabaseData requiredDataInformation)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    var neededPacket = db.DatabaseScientificDatas.Single(x => x.SatId == requiredDataInformation.PayloadId);
                    return new PayloadData(requiredDataInformation, neededPacket.Data.ToArray());
                }
            }
        }

        public void EditScientificPacket(IDatabaseData newPacket)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    var neededPacket = db.DatabaseScientificDatas.Single(x => x.SatId == newPacket.PayloadId);
                    neededPacket.Data = newPacket.Data;
                    db.SubmitChanges();
                }
            }
        }

        public void AcknowledgeTelecommand(uint commandId, BitmapParameter ackTypeParameter)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    var command = db.DatabaseTelecommands.Single(x => x.Id == commandId);
                    switch (ackTypeParameter.BitFields[0].optionValue)
                    {
                        case 0:
                            command.Received = true;
                            break;

                        case 1:
                            command.Completed = true;
                            break;

                        default:
                            break;
                    }
                    db.SubmitChanges();
                }
            }
        }

        #region Query Standard Packets
        public StandardTelemetry[] QueryStandardTelemetryPackets(DateTime start, DateTime final)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    var neededData = new List<DatabaseTelemetry>();
                    foreach (var packet in db.DatabaseTelemetries)
                    {
                        if (packet.GroundDateTime <= start) break;
                        if (packet.GroundDateTime < final)
                            neededData.Add(packet);
                    }
                    if (!neededData.Any()) return new StandardTelemetry[0];
                    else return neededData.Select(x => (StandardTelemetry)x).ToArray();
                }
            }
        }

        public StandardTelecommand[] QueryStandardTelecommandPackets(DateTime start, DateTime final)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    var neededData = new List<DatabaseTelecommand>();
                    foreach (var packet in db.DatabaseTelecommands)
                    {
                        if (packet.GroundDateTime <= start) break;
                        if (packet.GroundDateTime < final)
                            neededData.Add(packet);
                    }
                    if (!neededData.Any()) return new StandardTelecommand[0];
                    else return neededData.Select(x => (StandardTelecommand)x).ToArray();
                }
            }
        }

        public StandardTelemetry[] QueryStandardTelemetryPackets(int length, DateTime moment)
        {
            if (length == 0) throw new ArgumentException("Length is equal to 0", "length");
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    if (length > 0)
                    {
                        var neededData = new List<DatabaseTelemetry>();
                        foreach (var packet in db.DatabaseTelemetries)
                        {
                            if (packet.GroundDateTime <= moment) break;
                            neededData.Add(packet);
                        }
                        if (!neededData.Any()) return new StandardTelemetry[0];
                        else if (neededData.Count <= length) return neededData.Select(x => (StandardTelemetry)x).ToArray();
                        else return neededData.Skip(neededData.Count - length).Select(x => (StandardTelemetry)x).ToArray();
                    }
                    else
                    {
                        var neededData = new List<DatabaseTelemetry>();
                        foreach (var packet in db.DatabaseTelemetries)
                        {
                            if (neededData.Count == -1 * length) break;
                            if (packet.GroundDateTime < moment)
                                neededData.Add(packet);
                        }
                        if (!neededData.Any()) return new StandardTelemetry[0];
                        else return neededData.Select(x => (StandardTelemetry)x).ToArray();
                    }
                }
            }
        }

        public StandardTelecommand[] QueryStandardTelecommandPackets(int length, DateTime moment)
        {
            if (length == 0) throw new ArgumentException("Length is equal to 0", "length");
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    if (length > 0)
                    {
                        var neededData = new List<DatabaseTelecommand>();
                        foreach (var packet in db.DatabaseTelecommands)
                        {
                            if (packet.GroundDateTime <= moment) break;
                            neededData.Add(packet);
                        }
                        if (!neededData.Any()) return new StandardTelecommand[0];
                        else if (neededData.Count <= length) return neededData.Select(x => (StandardTelecommand)x).ToArray();
                        else return neededData.Skip(neededData.Count - length).Select(x => (StandardTelecommand)x).ToArray();
                    }
                    else
                    {
                        var neededData = new List<DatabaseTelecommand>();
                        foreach (var packet in db.DatabaseTelecommands)
                        {
                            if (neededData.Count == -1 * length) break;
                            if (packet.GroundDateTime < moment)
                                neededData.Add(packet);
                        }
                        if (!neededData.Any()) return new StandardTelecommand[0];
                        else return neededData.Select(x => (StandardTelecommand)x).ToArray();
                    }
                }
            }
        }
        #endregion

        #region Query Deciphered Packets
        public DecipheredPacket[] QueryDecipheredPackets(MissionInfromationController missionController, bool telemetryOrTelecommand)
        {
            lock (locker)
            {
                using (var database = new LINQtoSQLDataContext(connectionString))
                {
                    database.ObjectTrackingEnabled = false;
                    if (telemetryOrTelecommand)
                    {
                        return QueryDecipheredPackets_Telemetry(database.DatabaseTelemetries, missionController);
                    }
                    else
                    {
                        return QueryDecipheredPackets_Telecommand(database.DatabaseTelecommands, missionController);
                    }
                }
            }
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telemetry(IEnumerable<DatabaseTelemetry> telemetries, MissionInfromationController missionController)
        {
            ServiceTypeFormat[] format = missionController.FindFullMissionFormat(true);
            List<DatabaseTelemetry> neededData = new List<DatabaseTelemetry>();
            foreach (ServiceTypeFormat serviceType in format)
            {
                foreach (ServiceSubtypeFormat serviceSubtype in serviceType.ServiceSubtypes)
                {
                    DatabaseTelemetry telemetryLine = telemetries.First(x => x.ServiceType == serviceType.Value && x.ServiceSubtype == serviceSubtype.Value);
                    if (telemetryLine != null)
                    {
                        neededData.Add(telemetryLine);
                    }
                }
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else return Decipher(missionController, neededData);
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telecommand(IEnumerable<DatabaseTelecommand> telecommands, MissionInfromationController missionController)
        {
            ServiceTypeFormat[] format = missionController.FindFullMissionFormat(true);
            List<DatabaseTelecommand> neededData = new List<DatabaseTelecommand>();
            foreach (ServiceTypeFormat serviceType in format)
            {
                foreach (ServiceSubtypeFormat serviceSubtype in serviceType.ServiceSubtypes)
                {
                    DatabaseTelecommand telecommandLine = telecommands.First(x => x.ServiceType == serviceType.Value && x.ServiceSubtype == serviceSubtype.Value);
                    if (telecommandLine != null)
                    {
                        neededData.Add(telecommandLine);
                    }
                }
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else return Decipher(missionController, neededData);
        }


        public DecipheredPacket[] QueryDecipheredPackets(MissionInfromationController missionController, int length, bool telemetryOrTelecommand)
        {
            var c = Stopwatch.StartNew();
            if (length <= 0) throw new ArgumentException("Length is smaller or equal to 0", "length");
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    if (telemetryOrTelecommand)
                    {
                        if (!db.DatabaseTelemetries.Any()) return new DecipheredPacket[0];
                        else return Decipher(missionController, db.DatabaseTelemetries.Take(length));
                    }
                    else
                    {
                        if (!db.DatabaseTelecommands.Any()) return new DecipheredPacket[0];
                        else return Decipher(missionController, db.DatabaseTelecommands.Take(length));
                    }
                }
            }
        }


        public DecipheredPacket[] QueryDecipheredPackets(MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            if (length == 0) throw new ArgumentException("Length is equal to 0", "length");
            lock (locker)
            {
                using (var database = new LINQtoSQLDataContext(connectionString))
                {
                    database.ObjectTrackingEnabled = false;
                    if (telemetryOrTelecommand)
                    {
                        return QueryDecipheredPackets_Telemetry(database.DatabaseTelemetries, missionController, length, moment, serviceType, serviceSubtype);
                    }
                    else
                    {
                        return QueryDecipheredPackets_Telecommand(database.DatabaseTelecommands, missionController, length, moment, serviceType, serviceSubtype);
                    }
                }
            }
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telemetry(IEnumerable<DatabaseTelemetry> telemetries, MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype)
        {
            if (length > 0)
            {
                return QueryDecipheredPackets_Telemetry_Forward(telemetries, missionController, length, moment, serviceType, serviceSubtype);
            }
            else
            {
                return QueryDecipheredPackets_Telemetry_Backward(telemetries, missionController, -1 * length, moment, serviceType, serviceSubtype);
            }
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telemetry_Forward(IEnumerable<DatabaseTelemetry> telemetries, MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype)
        {
            var neededData = new List<DatabaseTelemetry>();
            foreach (var packet in telemetries)
            {
                if (packet.GroundDateTime <= moment) break;
                if (packet.ServiceType == serviceType && packet.ServiceSubtype == serviceSubtype)
                    neededData.Add(packet);
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else if (neededData.Count <= length) return Decipher(missionController, neededData);
            else return Decipher(missionController, neededData.Skip(neededData.Count - length));
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telemetry_Backward(IEnumerable<DatabaseTelemetry> telemetries, MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype)
        {
            var neededData = new List<DatabaseTelemetry>();
            foreach (var packet in telemetries)
            {
                if (neededData.Count == length) break;
                if (packet.GroundDateTime >= moment || packet.ServiceType != serviceType || packet.ServiceSubtype != serviceSubtype) continue;
                neededData.Add(packet);
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else return Decipher(missionController, neededData);
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telecommand(IEnumerable<DatabaseTelecommand> telecommands, MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype)
        {
            if (length > 0)
            {
                return QueryDecipheredPackets_Telecommand_Forward(telecommands, missionController, length, moment, serviceType, serviceSubtype);
            }
            else
            {
                return QueryDecipheredPackets_Telecommand_Backward(telecommands, missionController, length, moment, serviceType, serviceSubtype);
            }
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telecommand_Forward(IEnumerable<DatabaseTelecommand> telecommands, MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype)
        {
            var neededData = new List<DatabaseTelecommand>();
            foreach (var packet in telecommands)
            {
                if (packet.GroundDateTime <= moment) break;
                if (packet.ServiceType == serviceType && packet.ServiceSubtype == serviceSubtype)
                    neededData.Add(packet);
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else if (neededData.Count <= length) return Decipher(missionController, neededData);
            else return Decipher(missionController, neededData.Skip(neededData.Count - length));
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telecommand_Backward(IEnumerable<DatabaseTelecommand> telecommands, MissionInfromationController missionController, int length, DateTime moment, byte serviceType, byte serviceSubtype)
        {
            var neededData = new List<DatabaseTelecommand>();
            foreach (var packet in telecommands)
            {
                if (neededData.Count == length) break;
                if (packet.GroundDateTime >= moment || packet.ServiceType != serviceType || packet.ServiceSubtype != serviceSubtype) continue;
                neededData.Add(packet);
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else return Decipher(missionController, neededData);
        }


        public DecipheredPacket[] QueryDecipheredPackets(MissionInfromationController missionController, DateTime start, DateTime final, byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            lock (locker)
            {
                using (var database = new LINQtoSQLDataContext(connectionString))
                {
                    database.ObjectTrackingEnabled = false;
                    if (telemetryOrTelecommand)
                    {
                        return QueryDecipheredPackets_Telemetry(database.DatabaseTelemetries, missionController, start, final, serviceType, serviceSubtype);
                    }
                    else
                    {
                        return QueryDecipheredPackets_Telecommand(database.DatabaseTelecommands, missionController, start, final, serviceType, serviceSubtype);
                    }
                }
            }
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telemetry(IEnumerable<DatabaseTelemetry> telemetries, MissionInfromationController missionController, DateTime start, DateTime final, byte serviceType, byte serviceSubtype)
        {
            var neededData = new List<DatabaseTelemetry>();
            foreach (var packet in telemetries)
            {
                if (packet.GroundDateTime <= start) break;
                if (packet.GroundDateTime < final && packet.ServiceType == serviceType && packet.ServiceSubtype == serviceSubtype)
                    neededData.Add(packet);
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else return Decipher(missionController, neededData);
        }

        private DecipheredPacket[] QueryDecipheredPackets_Telecommand(IEnumerable<DatabaseTelecommand> telecommands, MissionInfromationController missionController, DateTime start, DateTime final, byte serviceType, byte serviceSubtype)
        {
            var neededData = new List<DatabaseTelecommand>();
            foreach (var packet in telecommands)
            {
                if (packet.GroundDateTime <= start) break;
                if (packet.GroundDateTime < final && packet.ServiceType == serviceType && packet.ServiceSubtype == serviceSubtype)
                    neededData.Add(packet);
            }
            if (!neededData.Any()) return new DecipheredPacket[0];
            else return Decipher(missionController, neededData);
        }
        #endregion

        #region Remove
        public void RemovePacket(int id, bool telemetryOrTelecommand)
        {
            bool notRemoved = true;
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    if (telemetryOrTelecommand)
                    {
                        foreach (var telemetry in db.DatabaseTelemetries)
                        {
                            if (telemetry.Id == id)
                            {
                                notRemoved = false;
                                db.DatabaseTelemetries.DeleteOnSubmit(telemetry);
                            }
                        }
                    }
                    else
                    {
                        foreach (var telecommand in db.DatabaseTelecommands)
                        {
                            if (telecommand.Id == id)
                            {
                                notRemoved = false;
                                db.DatabaseTelecommands.DeleteOnSubmit(telecommand);
                            }
                        }
                    }
                    if (notRemoved)
                    {
                        throw new DllNotFoundException(); // Create a better exception
                    }
                    db.SubmitChanges();
                }
            }
        }

        public void RemovePacket(DateTime start, DateTime finish, bool telemetryOrTelecommand)
        {
            bool notRemoved = true;
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    if (telemetryOrTelecommand)
                    {
                        foreach (var telemetry in db.DatabaseTelemetries)
                        {
                            if (telemetry.GroundDateTime >= start && telemetry.GroundDateTime <= finish)
                            {
                                notRemoved = false;
                                db.DatabaseTelemetries.DeleteOnSubmit(telemetry);
                            }
                        }
                    }
                    else
                    {
                        foreach (var telecommand in db.DatabaseTelecommands)
                        {
                            if (telecommand.GroundDateTime >= start && telecommand.GroundDateTime <= finish)
                            {
                                notRemoved = false;
                                db.DatabaseTelecommands.DeleteOnSubmit(telecommand);
                            }
                        }
                    }
                    if (notRemoved)
                    {
                        throw new DllNotFoundException();
                    }
                    db.SubmitChanges();
                }
            }
        }

        public void RemovePacket(DateTime start, DateTime finish, byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            bool notRemoved = true;
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    if (telemetryOrTelecommand)
                    {
                        foreach (var telemetry in db.DatabaseTelemetries)
                        {
                            if (telemetry.GroundDateTime >= start && telemetry.GroundDateTime <= finish
                                && telemetry.ServiceType == serviceType && telemetry.ServiceSubtype == serviceSubtype)
                            {
                                notRemoved = false;
                                db.DatabaseTelemetries.DeleteOnSubmit(telemetry);
                            }
                        }
                    }
                    else
                    {
                        foreach (var telecommand in db.DatabaseTelecommands)
                        {
                            if (telecommand.GroundDateTime >= start && telecommand.GroundDateTime <= finish
                                && telecommand.ServiceType == serviceType && telecommand.ServiceSubtype == serviceSubtype)
                            {
                                notRemoved = false;
                                db.DatabaseTelecommands.DeleteOnSubmit(telecommand);
                            }
                        }
                    }
                    if (notRemoved)
                    {
                        throw new DllNotFoundException();
                    }
                    db.SubmitChanges();
                }
            }
        }

        //public static void RemoveAll(bool telemetryOrTelecommand)
        //{
        //    using (var db = new LINQtoSQLDataContext(connectionString))
        //    {
        //        if (telemetryOrTelecommand)
        //        {
        //            db.Telemetries.;
        //        }
        //        else
        //        {
        //                    db.Telecommands.DeleteOnSubmit(telecommand);
        //        }
        //        if (notRemoved)
        //        {
        //            throw new DllNotFoundException();
        //        }
        //        db.SubmitChanges();
        //    }
        //}
        #endregion

        #region Support
        public void ReseedId(uint id, bool telemetryOrTelecommand)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    if (telemetryOrTelecommand)
                    {
                        db.ExecuteCommand("DBCC CHECKIDENT ('Telemetry', RESEED, " + id + ")"); // Must be 0 to set for identity seed
                    }
                    else
                    {
                        db.ExecuteCommand("DBCC CHECKIDENT ('Telecommand', RESEED, " + id + ")"); // Must be 999 to set for identity seed
                    }
                }
            }
        }

        public void ReseedIdScience(uint id)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ExecuteCommand("DBCC CHECKIDENT ('ScientificData', RESEED, " + id + ")");
                }
            }
        }

        public uint GetLastId(bool telemetryOrTelecommand)
        {
            lock (locker)
            {
                using (var db = new LINQtoSQLDataContext(connectionString))
                {
                    db.ObjectTrackingEnabled = false;
                    if (telemetryOrTelecommand)
                    {
                        return (uint)db.DatabaseTelemetries.First().Id;
                    }
                    else
                    {
                        return (uint)db.DatabaseTelecommands.First().Id;
                    }
                }
            }
        }
        #endregion

    }

    class PayloadData : IDatabaseData
    {
        public bool TelemetryOrTelecommand { get; set; }
        public byte ServiceType { get; set; }
        public byte ServiceSubtype { get; set; }
        public DateTime Time { get; set; }
        public uint PayloadId { get; set; }
        public byte[] Data { get; set; }

        public PayloadData(IDatabaseData initialRequest, byte[] data)
        {
            this.TelemetryOrTelecommand = initialRequest.TelemetryOrTelecommand;
            this.ServiceType = initialRequest.ServiceType;
            this.ServiceSubtype = initialRequest.ServiceSubtype;
            this.Time = initialRequest.Time;
            this.PayloadId = initialRequest.PayloadId;
            this.Data = data;
        }
    }
}
