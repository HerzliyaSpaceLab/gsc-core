﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HSL.GSC.Base.Data.SPL
{
    public class TelecommandPacket : Packet
    {
        public uint CommandId { get; private set; }

        public TelecommandPacket(uint commandId, byte serviceType, byte serviceSubtype, DateTime time, byte[] data)
            : base(serviceType, serviceSubtype, time, data)
        {
            this.CommandId = commandId;
        }
        public TelecommandPacket(byte[] bytes)
            : base(bytes.Skip(4).ToArray())
        {
            byte[] commandIdBytes = new byte[4];
            Array.Copy(bytes, 0, commandIdBytes, 0, commandIdBytes.Length);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(commandIdBytes);
            this.CommandId = BitConverter.ToUInt32(commandIdBytes, 0);
        }
        public TelecommandPacket(Storage.IStorageElement packet)
            : base(packet)
        {
            this.CommandId = (uint)packet.Id;
        }
        public TelecommandPacket(TelecommandPacket packet)
            : base(packet)
        {
            this.CommandId = packet.CommandId;
        }

        public override byte[] Encode()
        {
            List<byte> packet = new List<byte>();

            if (BitConverter.IsLittleEndian)
                packet.AddRange(BitConverter.GetBytes(this.CommandId).Reverse());
            else
                packet.AddRange(BitConverter.GetBytes(this.CommandId));

            packet.Add(this.ServiceType);
            packet.Add(this.ServiceSubtype);

            if (BitConverter.IsLittleEndian)
                packet.AddRange(BitConverter.GetBytes(this.DataLength).Reverse());
            else
                packet.AddRange(BitConverter.GetBytes(this.DataLength));

            packet.AddRange(this.TimeBytes);
            packet.AddRange(this.Data);

            return packet.ToArray();
        }
    }
}