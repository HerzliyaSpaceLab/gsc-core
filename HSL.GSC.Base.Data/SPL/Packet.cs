﻿using System;
using System.Linq;
using System.Collections.Generic;
using HSL.GSC.CommunicationFormat;

namespace HSL.GSC.Base.Data.SPL
{
    public abstract class Packet
    {
        public ushort DataLength { get; protected set; }
        public byte ServiceType { get; protected set; }
        public byte ServiceSubtype { get; protected set; }
        public DateTime Time { get; protected set; }
        public byte[] TimeBytes { get; protected set; }
        public byte[] Data { get; protected set; }

        protected Packet(byte serviceType, byte serviceSubtype, DateTime time, byte[] data)
        {
            this.ServiceType = serviceType;
            this.ServiceSubtype = serviceSubtype;
            this.DataLength = (ushort)data.Length;
            this.Time = time;
            this.TimeBytes = TimeDifferenceToBytes(this.Time);
            this.Data = new byte[this.DataLength];
            Array.Copy(data, this.Data, this.DataLength);
        }
        protected Packet(byte[] bytes)
        {
            this.ServiceType = bytes[0];
            this.ServiceSubtype = bytes[1];
            this.DataLength = (ushort)((bytes[2] << 8) | bytes[3]);
            this.TimeBytes = new byte[4];
            Array.Copy(bytes, 4, this.TimeBytes, 0, this.TimeBytes.Length);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(this.TimeBytes);
            this.Time = new DateTime(2000, 1, 1).AddSeconds(BitConverter.ToUInt32(this.TimeBytes, 0));
            this.Data = new byte[this.DataLength];
            Array.Copy(bytes, 8, this.Data, 0, this.Data.Length);
        }
        protected Packet(Storage.IStorageElement packet)
        {
            this.ServiceType = packet.ServiceType;
            this.ServiceSubtype = packet.ServiceSubtype;
            this.DataLength = (ushort)packet.Data.Length;
            this.Time = packet.GroundDateTime;
            this.TimeBytes = TimeDifferenceToBytes(this.Time);
            this.Data = new byte[this.DataLength];
            Array.Copy((byte[])packet.Data.ToArray().Clone(), this.Data, this.DataLength);
        }
        protected Packet(Packet packet)
        {
            this.DataLength = packet.DataLength;
            this.ServiceType = packet.ServiceType;
            this.ServiceSubtype = packet.ServiceSubtype;
            this.Time = packet.Time;
            this.TimeBytes = packet.TimeBytes;
            this.Data = new byte[packet.Data.Length];
            Array.Copy(packet.Data, this.Data, packet.Data.Length);
        }

        public override string ToString()
        {
            return BitConverter.ToString(Encode()).ToUpper();
        }
        public abstract byte[] Encode();
        protected byte[] TimeDifferenceToBytes(DateTime time)
        {
            uint unixTime = time.ToUnix();
            if (BitConverter.IsLittleEndian)
            {
                return BitConverter.GetBytes(unixTime).Reverse().ToArray();
            }
            return BitConverter.GetBytes(unixTime).ToArray();
        }
    }
}