﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HSL.GSC.Base.Data.SPL
{
    public class TelemetryPacket : Packet, API.IRawTelemetry
    {
        public TelemetryPacket(byte serviceType, byte serviceSubtype, DateTime time, byte[] data)
            : base(serviceType, serviceSubtype, time, data) { }
        public TelemetryPacket(byte[] bytes)
            : base(bytes) { }
        public TelemetryPacket(TelemetryPacket packet)
            : base(packet) { }

        public override byte[] Encode()
        {
            List<byte> packet = new List<byte>();
            packet.Add(this.ServiceType);
            packet.Add(this.ServiceSubtype);

            if (BitConverter.IsLittleEndian)
                packet.AddRange(BitConverter.GetBytes(this.DataLength).Reverse());
            else
                packet.AddRange(BitConverter.GetBytes(this.DataLength));

            packet.AddRange(this.TimeBytes);
            packet.AddRange(this.Data);

            return packet.ToArray();
        }
    }
}