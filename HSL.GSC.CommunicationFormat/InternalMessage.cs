﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat
{
    [Serializable]
    public sealed class InternalMessage : Message
    {
        public Guid DestinationGuid { get; private set; }
        public InternalMessage(Guid destinationGuid, MessageTypeEnum type)
            : base(type, new object())
        {
            this.DestinationGuid = destinationGuid;
        }
        public InternalMessage(Guid destinationGuid, MessageTypeEnum type, object content)
            : base(type, content)
        {
            this.DestinationGuid = destinationGuid;
        }
    }
}
