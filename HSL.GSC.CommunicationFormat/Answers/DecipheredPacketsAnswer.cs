﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSL.GSC.CommunicationFormat.Packets;

namespace HSL.GSC.CommunicationFormat.Answers
{
    [Serializable]
    public class DecipheredPacketsAnswer : Answer
    {
        public bool TelemetryOrTelecommand { get; private set; }
        public DecipheredPacket[] Data { get; private set; }

        public DecipheredPacketsAnswer(bool telemetryOrTelecommand, Guid requestId, DecipheredPacket[] data)
            : base(requestId)
        {
            this.Data = data;
        }
    }
}
