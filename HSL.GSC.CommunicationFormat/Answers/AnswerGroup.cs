﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Answers
{
    [Serializable]
    public class AnswerGroup
    {
        private object locker = new object();
        private Queue<Answer> requestsToHandle { get; set; }

        public byte[] GroupId { get; private set; }

        public AnswerGroup(Guid groupId)
        {
            requestsToHandle = new Queue<Answer>();
            this.GroupId = groupId.ToByteArray();
        }

        public void AddRequest(Answer request)
        {
            lock (locker)
            {
                requestsToHandle.Enqueue(request);
            }
        }

        public Answer GetNextRequest()
        {
            lock (locker)
            {
                if (requestsToHandle.Any())
                {
                    return requestsToHandle.Dequeue();
                }
                else
                {
                    return null;
                }
            }
        }

        public int GetRequestsCount()
        {
            lock (locker)
            {
                return requestsToHandle.Count;
            }
        }
    }
}
