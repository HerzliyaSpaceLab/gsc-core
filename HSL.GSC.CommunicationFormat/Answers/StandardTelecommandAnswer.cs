﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSL.GSC.CommunicationFormat.Packets;

namespace HSL.GSC.CommunicationFormat.Answers
{
    [Serializable]
    public class StandardTelecommandAnswer : Answer
    {
        public StandardTelecommand[] Data { get; private set; }

        public StandardTelecommandAnswer(Guid requestId, StandardTelecommand[] data)
            : base(requestId)
        {
            this.Data = data;
        }
    }
}
