﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Answers
{
    [Serializable]
    public abstract class Answer
    {
        public byte[] RequestId { get; protected set; }

        protected Answer(Guid requestId)
        {
            this.RequestId = requestId.ToByteArray();
        }
    }
}
