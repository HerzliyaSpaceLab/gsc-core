﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSL.GSC.CommunicationFormat.Packets;

namespace HSL.GSC.CommunicationFormat.Answers
{
    [Serializable]
    public class StandardTelemetryAnswer : Answer
    {
        public StandardTelemetry[] Data { get; private set; }

        public StandardTelemetryAnswer(Guid requestId, StandardTelemetry[] data)
            : base(requestId)
        {
            this.Data = data;
        }
    }
}
