﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat
{
    [Serializable]
    public class ClientIdentity
    {
        public enum ClientType
        {
            EndNode, GUI, Controller
        }

        public ClientType Type { get; private set; }

        public ClientIdentity(ClientType type)
        {
            this.Type = type;
        }
    }
}
