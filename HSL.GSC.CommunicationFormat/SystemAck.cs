﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat
{
    [Serializable]
    public class SystemAck
    {
        public enum SystemAckType
        {
            Error,
            ServerAck,
            ServerKick,
            ManagerAck,
            ManagerKick,
            Disconnection,
            DoneOperation
        }

        public SystemAckType type;
        public string message;

        public SystemAck(SystemAckType type, string message)
        {
            this.type = type;
            this.message = message;
        }
    }
}
