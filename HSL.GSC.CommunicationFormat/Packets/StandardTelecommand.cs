﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Packets
{
    [Serializable]
    public class StandardTelecommand : ICloneable
    {
        public long Id { get; set; }
        public DateTime GroundDateTime { get; set; }
        public byte ServiceType { get; set; }
        public byte ServiceSubtype { get; set; }
        public byte[] Data { get; set; }

        public StandardTelecommand(long id, DateTime groundDateTime, byte serviceType, byte serviceSubtype, byte[] data)
        {
            this.Id = id;
            this.GroundDateTime = groundDateTime;
            this.ServiceType = serviceType;
            this.ServiceSubtype = serviceSubtype;
            this.Data = data;
        }

        public static explicit operator StandardTelecommand(DecipheredPacket packet)
        {
            if (packet.TelemetryOrTelecommand)
            {
                throw new InvalidCastException();
            }
            return new StandardTelecommand(packet.Id, packet.GroundDateTime, packet.ServiceType, packet.ServiceSubtype, (byte[])packet.TotalRawData.Clone());
        }

        public object Clone()
        {
            return new StandardTelecommand(this.Id, this.GroundDateTime, this.ServiceType, this.ServiceSubtype, (byte[])this.Data.Clone());
        }
    }
}
