﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    public interface IRangeDefinable<T>
    {
        T RangeStart { get; }
        T RangeEnd { get; }
        bool IsInRange { get; }
    }
}
