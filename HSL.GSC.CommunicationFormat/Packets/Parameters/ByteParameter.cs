﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    [Serializable]
    public sealed class ByteParameter : Parameter, IRangeDefinable<byte>, IUnitDefinable
    {
        public byte RangeStart { get; private set; }
        public byte RangeEnd { get; private set; }
        public bool IsInRange { get; private set; }
        public string Unit { get; private set; }
        public override byte[] RawData
        {
            set
            {
                byte parsedValue = byte.Parse(Decipher(value));
                if (parsedValue > RangeStart && parsedValue < RangeEnd)
                    this.IsInRange = true;
                else
                    this.IsInRange = false;
                base.RawData = value;
            }
        }

        public ByteParameter(uint id, string name, string description, byte serviceType, byte serviceSubtype, byte rangeStart, byte rangeEnd, XElement calibration, string unit)
            : base(sizeof(byte), id, name, description, serviceType, serviceSubtype, calibration)
        {
            this.RangeStart = rangeStart;
            this.RangeEnd = rangeEnd;
            this.Unit = unit;
        }
        public ByteParameter(ByteParameter parameter)
            : base(sizeof(byte), parameter.Id, string.Copy(parameter.Name), string.Copy(parameter.Description), parameter.ServiceType, parameter.ServiceSubtype, parameter.Calibration)
        {
            this.RangeStart = parameter.RangeStart;
            this.RangeEnd = parameter.RangeEnd;
            this.Unit = string.Copy(parameter.Unit);
        }

        protected override string Decipher(byte[] rawData)
        {
            byte data = rawData[0];
            data = (byte)((Calibration as Calibrations.PolynomCalibration).A1 * rawData[0] + (Calibration as Calibrations.PolynomCalibration).A0);
            return Convert.ToString(data);
        }

        protected override byte[] Pack(string value)
        {
            byte convertedValue = byte.Parse(value);
            byte[] valueBytes = new byte[1] { convertedValue };
            return valueBytes;
        }
    }
}
