﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    [Serializable]
    public sealed class DoubleParameter : Parameter, IRangeDefinable<double>, IUnitDefinable, IEndianessDefinable
    {
        public double RangeStart { get; private set; }
        public double RangeEnd { get; private set; }
        public bool IsInRange { get; private set; }
        public string Unit { get; private set; }
        public bool IsLittleEndian { get; private set; }
        public override byte[] RawData
        {
            set
            {
                double parsedValue = double.Parse(Decipher(value));
                if (parsedValue > RangeStart && parsedValue < RangeEnd)
                    this.IsInRange = true;
                else
                    this.IsInRange = false;
                base.RawData = value;
            }
        }

        public DoubleParameter(uint id, string name, string description, byte serviceType, byte serviceSubtype, double rangeStart, double rangeEnd, XElement calibration, string unit, bool isLittleEndian)
            : base(sizeof(double), id, name, description, serviceType, serviceSubtype, calibration)
        {
            this.RangeStart = rangeStart;
            this.RangeEnd = rangeEnd;
            this.Unit = unit;
            this.IsLittleEndian = isLittleEndian;
        }
        public DoubleParameter(DoubleParameter parameter)
            : base(sizeof(double), parameter.Id, string.Copy(parameter.Name), string.Copy(parameter.Description), parameter.ServiceType, parameter.ServiceSubtype, parameter.Calibration)
        {
            this.RangeStart = parameter.RangeStart;
            this.RangeEnd = parameter.RangeEnd;
            this.Unit = string.Copy(parameter.Unit);
            this.IsLittleEndian = parameter.IsLittleEndian;
        }

        protected override string Decipher(byte[] rawData)
        {
            double data = default(double);

            if (IsLittleEndian == BitConverter.IsLittleEndian)
            {
                data = BitConverter.ToDouble(rawData, 0);
            }
            else
            {
                byte[] reversedData = (byte[])rawData.Clone();
                Array.Reverse(reversedData);
                data = BitConverter.ToDouble(reversedData, 0);
            }

            data = (Calibration as Calibrations.PolynomCalibration).A1 * data + (Calibration as Calibrations.PolynomCalibration).A0;

            return Convert.ToString(data);
        }

        protected override byte[] Pack(string value)
        {
            double convertedValue = double.Parse(value);
            byte[] valueBytes = BitConverter.GetBytes(convertedValue);
            if (this.IsLittleEndian != BitConverter.IsLittleEndian)
                Array.Reverse(valueBytes);
            return valueBytes;
        }
    }
}
