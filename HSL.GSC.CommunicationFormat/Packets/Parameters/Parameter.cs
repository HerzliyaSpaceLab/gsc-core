﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    [Serializable]
    public abstract class Parameter
    {
        private byte[] rawData;
        virtual public byte[] RawData
        {
            get => rawData;
            set
            {
                if (value.Length != Size)
                    throw new ArgumentException();
                rawData = value;
            }
        }
        public string Data { get => Decipher(this.RawData); set => this.RawData = Pack(value); }
        public int Size { get; private set; }
        public uint Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public Calibrations.Calibration Calibration { get; private set; }
        public byte ServiceType { get; private set; }
        public byte ServiceSubtype { get; private set; }
        public DateTime GroundDateTime { get; set; }
        public DateTime SatDateTime { get; set; }
        
        protected Parameter(int size, uint id, string name, string description, byte serviceType, byte serviceSubtype, XElement calibration = null)
        {
            this.Size = size;
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.ServiceType = serviceType;
            this.ServiceSubtype = serviceSubtype;

            if (calibration == null) this.Calibration = null;
            else if (this is FloatParameter || this is DoubleParameter || this is ByteParameter || this is IntParameter)
            {
                if (calibration.Attribute("type", true).Value.ToLowerInvariant() != "polynomial")
                {
                    throw new ArgumentException("Calibration is not polynomial");
                }
                this.Calibration = new Calibrations.PolynomCalibration(uint.Parse(calibration.Attribute("id", true).Value), calibration.Attribute("name", true).Value,
                    calibration.Attribute("function", true).Value, calibration.Attribute("solution", true).Value,
                    int.Parse(calibration.Attribute("a1", true).Value), int.Parse(calibration.Attribute("a0", true).Value));
            }
            else if (this is BitmapParameter)
            {
                this.Calibration = null;
            }
        }
        protected Parameter(int size, uint id, string name, string description, byte serviceType, byte serviceSubtype, Calibrations.Calibration calibration = null)
        {
            this.Size = size;
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.ServiceType = serviceType;
            this.ServiceSubtype = serviceSubtype;

            if (calibration == null) this.Calibration = null;
            else if (calibration is Calibrations.PolynomCalibration && (this is FloatParameter || this is DoubleParameter || this is ByteParameter || this is IntParameter))
            {
                this.Calibration = new Calibrations.PolynomCalibration(calibration as Calibrations.PolynomCalibration);
            }
            else if (this is BitmapParameter)
            {
                this.Calibration = null;
            }
        }
        protected abstract string Decipher(byte[] rawData);
        protected abstract byte[] Pack(string value);
    }

    
}
