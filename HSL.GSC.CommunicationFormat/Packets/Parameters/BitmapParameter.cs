﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    [Serializable]
    public sealed class BitmapParameter : Parameter
    {
        private Bitfield[] bitFields;

        public (string fieldName, string fieldDescription, string optionName, byte optionValue)[] BitFields
        {
            get
            {
                List<(string fieldName, string fieldDescription, string optionName, byte optionValue) > fieldsToReturn = new List<(string, string, string, byte)>();
                byte data = this.RawData[0];
                int remainedBitsAmount = Size * 8;

                foreach (var field in this.bitFields)
                {
                    remainedBitsAmount -= field.Size;
                    if (!field.IsNull)
                    {
                        byte optionValue = (byte)((data >> remainedBitsAmount) & ((1 << field.Size) - 1));
                        string optionName = field.Calibration.Options.Single(x => optionValue == x.Value).Name.Clone().ToString();
                        fieldsToReturn.Add((field.Name, field.Description, optionName, optionValue));
                    }
                }
                return fieldsToReturn.ToArray();
            }
        }

        public BitmapParameter(uint id, string name, string description, byte serviceType, byte serviceSubtype, Bitfield[] bitFields)
            : base(sizeof(byte), id, name, description, serviceType, serviceSubtype, null as XElement)
        {
            this.bitFields = bitFields;
        }

        protected override string Decipher(byte[] rawData)
        {
            string dataString = this.Name + " (" + this.Description + ")";
            int forUnderline = dataString.Length;
            dataString += Environment.NewLine;
            for (int idx = 0; idx < forUnderline; idx++)
            {
                dataString += "-";
            }
            dataString += Environment.NewLine + Environment.NewLine;
            var bitFieldsTuples = this.BitFields;
            foreach (var tuple in bitFieldsTuples)
            {
                dataString += tuple.fieldName + " (" + tuple.fieldDescription + "): " + tuple.optionName + Environment.NewLine;
            }
            return dataString;
        }

        protected override byte[] Pack(string value)
        {
            throw new NotImplementedException();
            /*int convertedValue = int.Parse(value);
            byte[] valueBytes = BitConverter.GetBytes(convertedValue);
            if (this.IsLittleEndian != BitConverter.IsLittleEndian)
                Array.Reverse(valueBytes);
            return valueBytes;*/
        }

        [Serializable]
        public sealed class Bitfield
        {
            public int Size { get; private set; }
            public bool IsNull { get; private set; }
            public string Name { get; private set; }
            public string Description { get; private set; }
            public Calibrations.OptionsCalibration Calibration { get; private set; }
            public Bitfield(int size, bool isNull, XElement calibration = null, string name = null, string description = null)
            {
                this.Size = size;
                this.IsNull = isNull;
                if (this.IsNull)
                {
                    this.Name = null;
                    this.Description = null;
                    this.Calibration = null;
                }
                else
                {
                    this.Name = name;
                    this.Description = description;
                    if (calibration.Attribute("type", true).Value.ToLowerInvariant() != "options")
                    {
                        throw new ArgumentException("Calibration is not options");
                    }
                    var options = calibration.Elements("option", true);
                    this.Calibration = new Calibrations.OptionsCalibration(uint.Parse(calibration.Attribute("id", true).Value), calibration.Attribute("name", true).Value,
                        options.Select(x => x.Attribute("name", true).Value).ToArray(),
                        options.Select(x => byte.Parse(x.Attribute("value", true).Value)).ToArray());
                }
            }
        }
    }
}
