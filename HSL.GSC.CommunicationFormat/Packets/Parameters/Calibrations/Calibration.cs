﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters.Calibrations
{
    [Serializable]
    public abstract class Calibration
    {
        public uint Id { get; private set; }
        public string Name { get; private set; }
        protected Calibration(uint id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}
