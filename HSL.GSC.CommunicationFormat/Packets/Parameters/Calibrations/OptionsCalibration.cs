﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters.Calibrations
{
    [Serializable]
    public sealed class OptionsCalibration : Calibration
    {
        public Option[] Options { get; private set; }
        public OptionsCalibration(uint id, string name, string[] names, byte[] values) : base(id, name)
        {
            if (names.Length != values.Length)
            {
                throw new InvalidOperationException();
            }
            Options = new Option[names.Length];
            for (int idx = 0; idx < Options.Length; idx++)
            {
                Options[idx] = new Option(names[idx], values[idx]);
            }
        }

        [Serializable]
        public sealed class Option
        {
            public string Name { get; private set; }
            public byte Value { get; private set; }
            public Option(string name, byte value)
            {
                this.Name = name;
                this.Value = value;
            }
        }
    }
}
