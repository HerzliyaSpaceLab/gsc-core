﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters.Calibrations
{
    [Serializable]
    public sealed class PolynomCalibration : Calibration
    {
        public int A0 { get; private set; }
        public int A1 { get; private set; }
        public string Function { get; private set; }
        public string Solution { get; private set; }
        public PolynomCalibration(uint id, string name, string function, string solution, int a1, int a0) : base(id, name)
        {
            this.Function = function;
            this.Solution = solution;
            this.A0 = a0;
            this.A1 = a1;
        }
        public PolynomCalibration(PolynomCalibration calibration) : base(calibration.Id, string.Copy(calibration.Name))
        {
            this.Function = string.Copy(calibration.Function);
            this.Solution = string.Copy(calibration.Solution);
            this.A0 = calibration.A0;
            this.A1 = calibration.A1;
        }
    }
}
