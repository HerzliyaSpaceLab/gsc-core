﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    public sealed class DateTimeParameter : Parameter, IRangeDefinable<DateTime>, IUnitDefinable, IEndianessDefinable
    {
        public DateTime RangeStart { get; private set; }
        public DateTime RangeEnd { get; private set; }
        public bool IsInRange { get; private set; }
        public string Unit { get; private set; }
        public bool IsLittleEndian { get; private set; }
        public override byte[] RawData
        {
            set
            {
                DateTime parsedValue = DateTime.Parse(Decipher(value));
                if (parsedValue > RangeStart && parsedValue < RangeEnd)
                    this.IsInRange = true;
                else
                    this.IsInRange = false;
                base.RawData = value;
            }
        }

        public DateTimeParameter(uint id, string name, string description, byte serviceType, byte serviceSubtype, DateTime rangeStart, DateTime rangeEnd, XElement calibration, string unit, bool isLittleEndian)
            : base(sizeof(long), id, name, description, serviceType, serviceSubtype, calibration)
        {
            this.RangeStart = rangeStart;
            this.RangeEnd = rangeEnd;
            this.Unit = unit;
            this.IsLittleEndian = isLittleEndian;
        }
        public DateTimeParameter(DateTimeParameter parameter)
            : base(sizeof(double), parameter.Id, string.Copy(parameter.Name), string.Copy(parameter.Description), parameter.ServiceType, parameter.ServiceSubtype, parameter.Calibration)
        {
            this.RangeStart = parameter.RangeStart;
            this.RangeEnd = parameter.RangeEnd;
            this.Unit = string.Copy(parameter.Unit);
            this.IsLittleEndian = parameter.IsLittleEndian;
        }

        protected override string Decipher(byte[] rawData)
        {
            long data = default(long);

            if (IsLittleEndian == BitConverter.IsLittleEndian)
            {
                data = BitConverter.ToInt64(rawData, 0);
            }
            else
            {
                byte[] reversedData = (byte[])rawData.Clone();
                Array.Reverse(reversedData);
                data = BitConverter.ToInt64(reversedData, 0);
            }

            data = (uint)((Calibration as Calibrations.PolynomCalibration).A1 * data + (Calibration as Calibrations.PolynomCalibration).A0);

            return DateTime.FromBinary(data).ToString();
        }

        protected override byte[] Pack(string value)
        {
            DateTime time = DateTime.ParseExact(value, "", null);
            uint unixTime = time.ToUnix();
            if (IsLittleEndian == BitConverter.IsLittleEndian)
                return BitConverter.GetBytes(unixTime).ToArray();
            return BitConverter.GetBytes(unixTime).Reverse().ToArray();
        }
    }
}
