﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    [Serializable]
    public sealed class FloatParameter : Parameter, IRangeDefinable<float>, IUnitDefinable, IEndianessDefinable
    {
        public float RangeStart { get; private set; }
        public float RangeEnd { get; private set; }
        public bool IsInRange { get; private set; }
        public string Unit { get; private set; }
        public bool IsLittleEndian { get; private set; }
        public override byte[] RawData
        {
            set
            {
                float parsedValue = float.Parse(Decipher(value));
                if (parsedValue > RangeStart && parsedValue < RangeEnd)
                    this.IsInRange = true;
                else
                    this.IsInRange = false;
                base.RawData = value;
            }
        }

        public FloatParameter(uint id, string name, string description, byte serviceType, byte serviceSubtype, float rangeStart, float rangeEnd, XElement calibration, string unit, bool isLittleEndian)
            : base(sizeof(float), id, name, description, serviceType, serviceSubtype, calibration)
        {
            this.RangeStart = rangeStart;
            this.RangeEnd = rangeEnd;
            this.Unit = unit;
            this.IsLittleEndian = isLittleEndian;
        }
        public FloatParameter(FloatParameter parameter)
            : base(sizeof(float), parameter.Id, string.Copy(parameter.Name), string.Copy(parameter.Description), parameter.ServiceType, parameter.ServiceSubtype, parameter.Calibration)
        {
            this.RangeStart = parameter.RangeStart;
            this.RangeEnd = parameter.RangeEnd;
            this.Unit = string.Copy(parameter.Unit);
            this.IsLittleEndian = parameter.IsLittleEndian;
        }

        protected override string Decipher(byte[] rawData)
        {
            float data = default(float);

            if (IsLittleEndian == BitConverter.IsLittleEndian)
            {
                data = BitConverter.ToSingle(rawData, 0);
            }
            else
            {
                byte[] reversedData = (byte[])rawData.Clone();
                Array.Reverse(reversedData);
                data = BitConverter.ToSingle(reversedData, 0);
            }

            data = (Calibration as Calibrations.PolynomCalibration).A1 * data + (Calibration as Calibrations.PolynomCalibration).A0;

            return Convert.ToString(data);
        }

        protected override byte[] Pack(string value)
        {
            float convertedValue = float.Parse(value);
            byte[] valueBytes = BitConverter.GetBytes(convertedValue);
            if (this.IsLittleEndian != BitConverter.IsLittleEndian)
                Array.Reverse(valueBytes);
            return valueBytes;
        }
    }
}
