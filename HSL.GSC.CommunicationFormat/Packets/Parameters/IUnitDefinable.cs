﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Packets.Parameters
{
    public interface IUnitDefinable
    {
        string Unit { get; }
    }
}
