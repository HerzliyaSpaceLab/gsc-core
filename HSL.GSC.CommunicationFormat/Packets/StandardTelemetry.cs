﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Packets
{
    [Serializable]
    public class StandardTelemetry : ICloneable
    {
        public long Id { get; set; }
        public DateTime GroundDateTime { get; set; }
        public DateTime SatDateTime { get; set; }
        public byte ServiceType { get; set; }
        public byte ServiceSubtype { get; set; }
        public byte[] Data { get; set; }

        public StandardTelemetry(long id, DateTime groundDateTime, DateTime satDateTime, byte serviceType, byte serviceSubtype, byte[] data)
        {
            this.Id = id;
            this.GroundDateTime = groundDateTime;
            this.SatDateTime = satDateTime;
            this.ServiceType = serviceType;
            this.ServiceSubtype = serviceSubtype;
            this.Data = data;
        }

        public object Clone()
        {
            return new StandardTelemetry(this.Id, this.GroundDateTime, this.SatDateTime, this.ServiceType, this.ServiceSubtype, (byte[])this.Data.Clone());
        }
    }
}
