﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using HSL.GSC.CommunicationFormat.MissionFormat;

namespace HSL.GSC.CommunicationFormat.Packets
{
    [Serializable]
    public sealed class DecipheredPacket
    {
        public long Id { get; set; }
        public bool TelemetryOrTelecommand { get; private set; }
        public string Name { get; private set; }
        public byte ServiceType { get; private set; }
        public byte ServiceSubtype { get; private set; }
        public DateTime GroundDateTime { get; set; }
        public DateTime SatDateTime { get; set; }
        public Parameter[] Parameters { get; set; }
        public byte[] TotalRawData
        {
            get
            {
                return Parameters.Select(x => x.RawData).Aggregate((current, next) => current.Concat(next).ToArray());
            }
        }
        public DecipheredPacket(long id, bool telemetryOrTelecommand, string name, byte serviceType, byte serviceSubtype, DateTime groundDateTime, DateTime satDateTime, Parameter[] parameters, bool isFull)
        {
            this.Id = id;
            this.TelemetryOrTelecommand = telemetryOrTelecommand;
            this.Name = name;
            this.ServiceType = serviceType;
            this.ServiceSubtype = serviceSubtype;
            this.GroundDateTime = groundDateTime;
            this.SatDateTime = satDateTime;
            this.Parameters = parameters;
        }
        public DecipheredPacket(DecipheredPacket packet)
        {
            this.Id = packet.Id;
            this.TelemetryOrTelecommand = packet.TelemetryOrTelecommand;
            this.Name = string.Copy(packet.Name);
            this.ServiceType = packet.ServiceType;
            this.ServiceSubtype = packet.ServiceSubtype;
            this.GroundDateTime = packet.GroundDateTime;
            this.SatDateTime = packet.SatDateTime;
            this.Parameters = packet.Parameters.Select(parameter =>
            {
                if (parameter is FloatParameter)
                {
                    return new FloatParameter(parameter as FloatParameter) as Parameter;
                }
                else if (parameter is DoubleParameter)
                {
                    return new DoubleParameter(parameter as DoubleParameter) as Parameter;
                }
                else if (parameter is ByteParameter)
                {
                    return new ByteParameter(parameter as ByteParameter) as Parameter;
                }
                else if (parameter is IntParameter)
                {
                    return new IntParameter(parameter as IntParameter) as Parameter;
                }
                //else if (parameter is BitmapParameter)
                //{
                //    return XElementToBitmapParameter(parameterNode, serviceType, serviceSubtype, isLittleEndian, telemetryOrTelecommand);
                //}
                else
                {
                    return null as Parameter;
                }
            }).ToArray();
        }

        public DecipheredPacket(ServiceSubtypeFormat format, long id, DateTime groundDateTime, DateTime satDateTime, Parameter[] parameters)
        {
            this.Id = Id;
            this.TelemetryOrTelecommand = format.RootServiceType.TelemetryOrTelecommand;
            this.Name = format.RootServiceType.ToString();
            this.ServiceType = format.RootServiceType.Value;
            this.ServiceSubtype = format.Value;
            this.GroundDateTime = groundDateTime;
            this.SatDateTime = satDateTime;
            this.Parameters = parameters;
            //this.Parameters = format.Parameters.Select(x => new Parameter()).ToArray();
        }
    }
}
