﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HSL.GSC.CommunicationFormat
{
    public enum MessageTypeEnum
    {
        RawTelemetry,
        StandardTelemetry,
        DecipheredTelemetry,

        RawTelecommand,
        StandardTelecommand,
        DecipheredTelecommand,

        ClientIdentity,
        SystemAck,
        SystemRequest,
        RequestAnswer,

        TelemetryMissionFormat,
        TelecommandMissionFormat
    }

    public static class XElementExtensions
    {
        public static XElement Element(this XElement element, XName name, bool ignoreCase)
        {
            var el = element.Element(name);
            if (el != null)
                return el;
            if (!ignoreCase)
                return null;
            var elements = element.Elements().Where(e => e.Name.LocalName.ToString().ToLowerInvariant() == name.ToString().ToLowerInvariant());
            return elements.FirstOrDefault();
        }

        public static IEnumerable<XElement> Elements(this XElement element, XName name, bool ignoreCase)
        {
            var el = element.Elements(name);
            if (el.Any())
                return el;
            if (!ignoreCase)
                return null;
            var elements = element.Elements().Where(e => e.Name.LocalName.ToString().ToLowerInvariant() == name.ToString().ToLowerInvariant());
            return elements;
        }

        public static XAttribute Attribute(this XElement element, XName name, bool ignoreCase)
        {
            var at = element.Attribute(name);
            if (at != null)
                return at;
            if (!ignoreCase)
                return null;
            var elements = element.Attributes().Where(e => e.Name.LocalName.ToString().ToLowerInvariant() == name.ToString().ToLowerInvariant());
            return elements.FirstOrDefault();
        }
    }

    public static class DateTimeExtensions
    {
        public static uint ToUnix(this DateTime dateTime)
        {
            TimeSpan timeDifference = dateTime.Subtract(new DateTime(1970, 1, 1));
            uint totalSeconds = (uint)Math.Floor(timeDifference.TotalSeconds);
            return totalSeconds;
        }
    }
}
