﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace HSL.GSC.CommunicationFormat
{
    [Serializable]
    public abstract class Message
    {
        public MessageTypeEnum Type { get; protected set; }
        //protected byte[] content;
        //public object Content
        //{
        //    get
        //    {
        //        object contentObject = null;
        //        if (content.Length > 0)
        //        {
        //            using (var tempStream = new MemoryStream(content))
        //            {
        //                BinaryFormatter formatter = new BinaryFormatter();
        //                contentObject = formatter.Deserialize(tempStream);
        //            }
        //        }
        //        return contentObject;
        //    }
        //    protected set
        //    {
        //        if (value != null)
        //        {
        //            using (var tempStream = new MemoryStream())
        //            {
        //                BinaryFormatter formatter = new BinaryFormatter();
        //                formatter.Serialize(tempStream, value);
        //                this.content = new byte[tempStream.Position];
        //                tempStream.Position = 0;
        //                tempStream.Read(this.content, 0, this.content.Length);
        //            }
        //        }
        //    }
        //}
        public object Content { get; set; }
        protected Message(MessageTypeEnum type, object content)
        {
            this.Type = type;
            if (content == null)
            {
                this.Content = new object();
            }
            else
            {
                this.Content = content;
            }
        }
    }
}
