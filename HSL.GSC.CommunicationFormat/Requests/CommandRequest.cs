﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Requests
{
    [Serializable]
    public class CommandRequest : Request
    {
        public string EndNode { get; private set; }
        public Packets.DecipheredPacket Command { get; private set; }
        public CommandRequest(Guid requestId, string endNode, Packets.DecipheredPacket command)
            : base(requestId)
        {
            this.EndNode = endNode;
            this.Command = command;
        }
    }
}
