﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Requests
{
    [Serializable]
    public class ChangeRequest : Request
    {
        public ChangeRequest(Guid requestId)
            : base(requestId)
        {
        }
    }
}
