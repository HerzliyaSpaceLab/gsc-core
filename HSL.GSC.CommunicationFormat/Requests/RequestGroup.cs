﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.Requests
{
    [Serializable]
    public class RequestGroup
    {
        private object locker = new object();
        private Queue<Request> requestsToHandle { get; set; }

        public byte[] GroupId { get; private set; }

        public RequestGroup(Guid groupId)
        {
            requestsToHandle = new Queue<Request>();
            this.GroupId = groupId.ToByteArray();
        }

        public void AddRequest(Request request)
        {
            lock (locker)
            {
                requestsToHandle.Enqueue(request);
            }
        }

        public Request GetNextRequest()
        {
            lock (locker)
            {
                if (requestsToHandle.Any())
                {
                    return requestsToHandle.Dequeue();
                }
                else
                {
                    return null;
                }
            }
        }

        public int GetRequestsCount()
        {
            lock (locker)
            {
                return requestsToHandle.Count;
            }
        }
    }
}
