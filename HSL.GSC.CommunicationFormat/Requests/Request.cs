﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Requests
{
    [Serializable]
    public abstract class Request
    {
        public byte[] RequestId { get; protected set; }
        protected List<object> Parameters { get; set; }
        public List<object> ParameterValues
        {
            get
            {
                return new List<object>(Parameters);
            }
        }

        protected Request(Guid requestId)
        {
            this.RequestId = requestId.ToByteArray();
        }
    }
}
