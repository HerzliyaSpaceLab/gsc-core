﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat.Requests
{
    [Serializable]
    public class DecipheredPacketsRequest : Request
    {
        public enum ParameterType
        {
            Length,
            Moment,
            StartTime,
            FinalTime,
            ServiceType,
            ServiceSubtype
        }
        public bool TelemetryOrTelecommand { get; private set; }
        private List<ParameterType> Types { get; set; }
        public List<ParameterType> ParameterTypes
        {
            get
            {
                return new List<ParameterType>(Types);
            }
        }
        public DecipheredPacketsRequest(bool telemetryOrTelecommand, Guid requestId, ParameterType type, object value)
            : base(requestId)
        {
            this.TelemetryOrTelecommand = telemetryOrTelecommand;
            Types = new List<ParameterType>();
            Parameters = new List<object>();
            AddParameter(type, value);
        }
        public DecipheredPacketsRequest(bool telemetryOrTelecommand, Guid requestId, ParameterType[] typesArray, object[] valuesArray)
            : base(requestId)
        {
            this.TelemetryOrTelecommand = telemetryOrTelecommand;
            Types = new List<ParameterType>();
            Parameters = new List<object>();
            AddParameters(typesArray, valuesArray);
        }

        public static DecipheredPacketsRequest Create(bool telemetryOrTelecommand)
        {
            object[] obj = new object[0];
            DecipheredPacketsRequest.ParameterType[] type = new DecipheredPacketsRequest.ParameterType[0];

            var requestId = Guid.NewGuid();
            DecipheredPacketsRequest request = new DecipheredPacketsRequest(telemetryOrTelecommand, requestId, type, obj);
            return request;
        }

        public static DecipheredPacketsRequest Create(bool telemetryOrTelecommand, int length)
        {
            object[] obj = new object[1];
            DecipheredPacketsRequest.ParameterType[] type = new DecipheredPacketsRequest.ParameterType[1];
            type[0] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.Length;
            obj[0] = length;

            var requestId = Guid.NewGuid();
            DecipheredPacketsRequest request = new DecipheredPacketsRequest(telemetryOrTelecommand, requestId, type, obj);
            return request;
        }

        public static DecipheredPacketsRequest Create(bool telemetryOrTelecommand, DateTime startTime, DateTime endTime, byte serviceType, byte serviceSubtype)
        {
            object[] obj = new object[4];
            DecipheredPacketsRequest.ParameterType[] type = new DecipheredPacketsRequest.ParameterType[4];

            obj[0] = startTime;
            type[0] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.StartTime;
            obj[1] = endTime;
            type[1] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.FinalTime;
            obj[2] = serviceType;
            type[2] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.ServiceType;
            obj[3] = serviceSubtype;
            type[3] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.ServiceSubtype;

            var requestId = Guid.NewGuid();
            DecipheredPacketsRequest request = new DecipheredPacketsRequest(telemetryOrTelecommand, requestId, type, obj);
            return request;
        }

        public static DecipheredPacketsRequest Create(bool telemetryOrTelecommand, DateTime moment, int length, byte serviceType, byte serviceSubtype)
        {
            object[] obj = new object[4];
            DecipheredPacketsRequest.ParameterType[] type = new DecipheredPacketsRequest.ParameterType[4];

            obj[0] = moment;
            type[0] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.Moment;
            obj[1] = length;
            type[1] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.Length;
            obj[2] = serviceType;
            type[2] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.ServiceType;
            obj[3] = serviceSubtype;
            type[3] = CommunicationFormat.Requests.DecipheredPacketsRequest.ParameterType.ServiceSubtype;

            var requestId = Guid.NewGuid();
            DecipheredPacketsRequest request = new DecipheredPacketsRequest(telemetryOrTelecommand, requestId, type, obj);
            return request;
        }

        public void AddParameter(ParameterType type, object value)
        {
            if (!EnumChecker(type, value))
            {
                throw new Exception();
            }
            Types.Add(type);
            Parameters.Add(value);
        }
        public void AddParameters(ParameterType[] typesArray, object[] valuesArray)
        {
            if (typesArray.Length != valuesArray.Length)
            {
                throw new Exception();
            }
            for (int idx = 0; idx < typesArray.Length; idx++)
            {
                if (!EnumChecker(typesArray[idx], valuesArray[idx]))
                {
                    throw new Exception();
                }
                Types.Add(typesArray[idx]);
                Parameters.Add(valuesArray[idx]);
            }
        }
        private bool EnumChecker(ParameterType type, object value)
        {
            switch (type)
            {
                case ParameterType.Length:
                    if (!(value is int)) return false;
                    return true;
                case ParameterType.Moment:
                    if (!(value is DateTime)) return false;
                    return true;
                case ParameterType.StartTime:
                    if (!(value is DateTime)) return false;
                    return true;
                case ParameterType.FinalTime:
                    if (!(value is DateTime)) return false;
                    return true;
                case ParameterType.ServiceType:
                    if (!(value is byte)) return false;
                    return true;
                case ParameterType.ServiceSubtype:
                    if (!(value is byte)) return false;
                    return true;
                default:
                    throw new Exception();
            }
        }
    }
}
