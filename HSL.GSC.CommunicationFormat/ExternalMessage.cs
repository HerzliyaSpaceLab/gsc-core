﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSL.GSC.CommunicationFormat
{
    [Serializable]
    public sealed class ExternalMessage : Message
    {
        public ExternalMessage(MessageTypeEnum type)
            : base(type, new object())
        {
        }
        public ExternalMessage(MessageTypeEnum type, object content)
            : base(type, content)
        {
        }
        public ExternalMessage(InternalMessage message)
            : base(message.Type, message.Content)
        {
        }
    }
}
