﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.MissionFormat
{
    [Serializable]
    public class ParameterFormat
    {
        public uint Id { get; set; }
        public uint PreviousId { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLittleEndian { get; set; }
        public string Unit { get; set; }
        public ServiceSubtypeFormat RootServiceSubtype { get; internal set; }
        // type
        // calibration
        // rangestart
        // rangeend

        public ParameterFormat(uint id, string name, string description, bool isLittleEndian, string unit)
        {
            this.PreviousId = this.Id = id;
            this.Name = name;
            this.Description = description;
            this.Unit = unit;
            this.RootServiceSubtype = null;
        }

        public void UpdatePreviousID()
        {
            this.PreviousId = this.Id;
        }

        public object Clone()
        {
            return new ParameterFormat(this.Id, (string)this.Name.Clone(), (string)this.Description.Clone(), this.IsLittleEndian, (string)this.Unit.Clone())
            {
                RootServiceSubtype = this.RootServiceSubtype
            };
        }
    }
}
