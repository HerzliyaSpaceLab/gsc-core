﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.MissionFormat
{
    [Serializable]
    public class ServiceTypeFormat
    {
        public bool TelemetryOrTelecommand { get; private set; }
        public string Name { get; set; }
        public byte Value { get; set; }
        public byte PreviousValue { get; private set; }
        public List<ServiceSubtypeFormat> ServiceSubtypes { get; private set; }

        public ServiceTypeFormat(bool telemetryOrTelecommand, string name, byte value, List<ServiceSubtypeFormat> serviceSubtypes)
        {
            this.TelemetryOrTelecommand = telemetryOrTelecommand;
            this.Name = name;
            this.PreviousValue = this.Value = value;
            this.ServiceSubtypes = serviceSubtypes;
            foreach (ServiceSubtypeFormat serviceSubtype in this.ServiceSubtypes)
            {
                serviceSubtype.RootServiceType = this;
            }
        }

        public void UpdatePreviousID()
        {
            this.PreviousValue = this.Value;
        }

        public void UpdatePreviousIDsRecursively()
        {
            this.PreviousValue = this.Value;
            foreach (ServiceSubtypeFormat subtype in this.ServiceSubtypes)
            {
                subtype.UpdatePreviousIDsRecursively();
            }
        }

        public object Clone()
        {
            return new ServiceTypeFormat(this.TelemetryOrTelecommand, (string)this.Name.Clone(), this.Value, this.ServiceSubtypes.Select(x => (ServiceSubtypeFormat)x.Clone()).ToList());
        }
    }
}
