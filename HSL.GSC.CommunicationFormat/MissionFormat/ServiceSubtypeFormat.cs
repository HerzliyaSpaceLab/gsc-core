﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.CommunicationFormat.MissionFormat
{
    [Serializable]
    public class ServiceSubtypeFormat
    {
        public string Name { get; set; }
        public byte Value { get; set; }
        public byte PreviousValue { get; private set; }
        public ServiceTypeFormat RootServiceType { get; internal set; }
        public List<ParameterFormat> Parameters { get; private set; }

        public ServiceSubtypeFormat(string name, byte value, List<ParameterFormat> parameters)
        {
            this.Name = name;
            this.PreviousValue = this.Value = value;
            this.Parameters = new List<ParameterFormat>();
            this.RootServiceType = null;
            this.Parameters = parameters;
            foreach (ParameterFormat parameter in this.Parameters)
            {
                parameter.RootServiceSubtype = this;
            }
        }

        public void UpdatePreviousID()
        {
            this.PreviousValue = this.Value;
        }

        public void UpdatePreviousIDsRecursively()
        {
            this.PreviousValue = this.Value;
            foreach (ParameterFormat parameter in this.Parameters)
            {
                parameter.UpdatePreviousID();
            }
        }

        public object Clone()
        {
            return new ServiceSubtypeFormat((string)this.Name.Clone(), this.Value, this.Parameters.Select(x => (ParameterFormat)x.Clone()).ToList())
            {
                RootServiceType = this.RootServiceType
            };
        }

        public override string ToString()
        {
            return RootServiceType.Name + ": " + this.Name;
        }
    }
}
