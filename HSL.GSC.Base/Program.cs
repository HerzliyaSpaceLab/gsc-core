﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using HSL.GSC.CommunicationFormat.Packets;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using HSL.GSC.Base.Data.Storage;
using System.Configuration;
using System.IO;
using Moq;

namespace HSL.GSC.Base
{
    public static class Program
    {
        static KeyValueConfigurationCollection settings;
        static BaseApplicationContext context;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            bool debug = default(bool);
            byte ackServiceType = default(byte), ackServiceSubtype = default(byte);
            string pluginsLocation = default(string), storageLocation = default(string), missionInformationLocation = default(string);
            ushort serverPort = default(ushort);

            if (settings != null)
            {
                debug = bool.Parse(settings["DebugMode"].Value);
                ackServiceType = byte.Parse(settings["AckServiceType"].Value);
                ackServiceSubtype = byte.Parse(settings["AckServiceSubtype"].Value);
                pluginsLocation = settings["PluginsLocation"].Value;
                storageLocation = settings["StorageLocation"].Value;
                missionInformationLocation = settings["MissionInformationLocation"].Value;
                serverPort = ushort.Parse(settings["ServerPort"].Value);
            }
            else
            {
                debug = bool.Parse(ConfigurationManager.AppSettings["DebugMode"]);
                ackServiceType = byte.Parse(ConfigurationManager.AppSettings["AckServiceType"]);
                ackServiceSubtype = byte.Parse(ConfigurationManager.AppSettings["AckServiceSubtype"]);
                pluginsLocation = ConfigurationManager.AppSettings["PluginsLocation"]; // @"E:\Main\Satellites\My Real Work\GSC\Code\Base\HSL.GSC.Base\bin\Debug\Plugins"
                storageLocation = ConfigurationManager.AppSettings["StorageLocation"]; // @"E:\Main\Satellites\My Real Work\GSC\Code\Base\HSL.GSC.Base\bin\Debug\Database\Database.mdf"
                missionInformationLocation = ConfigurationManager.AppSettings["MissionInformationLocation"]; // @"E:\Main\Satellites\My Real Work\GSC\Code\Base\HSL.GSC.Base\bin\Debug\MissionInformation\MIB.xml"
                serverPort = ushort.Parse(ConfigurationManager.AppSettings["ServerPort"]);
            }

            context = new BaseApplicationContext(debug, pluginsLocation, storageLocation, missionInformationLocation, serverPort, ackServiceType, ackServiceSubtype);
            Console.WriteLine("@@@@@ ACTIVATED @@@@@");

            //while (!context.Disposed) ;
            /*Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new BaseApplicationContext(true));*/
        }

        /// <summary>
        /// The backdoor entry point for the application.
        /// </summary>
        /// <param name="configurationPath">Path to the .exe file of the Base (to get configuration)</param>
        public static void BackdoorEntry(string configurationPath = @"E:\Main\Satellites\My Real Work\GSC\Code\Base\HSL.GSC.Base\bin\Debug\HSL.GSC.Base.exe")
        {
            settings = ConfigurationManager.OpenExeConfiguration(configurationPath).AppSettings.Settings;
            Main();
        }

        public static DecipheredPacket FindCertainFormat(byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            return context.FindCertainFormat(serviceType, serviceSubtype, telemetryOrTelecommand);
        }

        public static void RequestParametersInputCsv(DecipheredPacket packetFormat, string csvPath)
        {
            var fileStream = new FileStream(csvPath, FileMode.CreateNew, FileAccess.Write);
            var writer = new StreamWriter(fileStream);

            writer.WriteLine("Name,Description,Type,Start Range,End Range,Endianess,Unit,Enter ~Value~ Here");

            foreach (var parameter in packetFormat.Parameters)
            {
                string line = parameter.Name + "," + parameter.Description + "," + parameter.GetType().Name + ",";

                if (parameter is IRangeDefinable<object>)
                    line += (parameter as IRangeDefinable<object>).RangeStart + "," + (parameter as IRangeDefinable<object>).RangeEnd + ",";
                else
                    line += "none,none,";

                if (parameter is IEndianessDefinable)
                {
                    if ((parameter as IEndianessDefinable).IsLittleEndian)
                        line += "little-endian,";
                    else
                        line += "big-endian,";
                }
                else
                    line += "none,";

                if (parameter is IUnitDefinable)
                    line += (parameter as IUnitDefinable).Unit + ",";
                else
                    line += "none,";

                writer.WriteLine(line);
            }

            writer.Close();
        }

        public static DecipheredPacket HandleParameters(DecipheredPacket packetFormat, string csvPath)
        {
            DecipheredPacket copy = new DecipheredPacket(packetFormat);
            var fileStream = new FileStream(csvPath, FileMode.Open, FileAccess.Read);
            var reader = new StreamReader(fileStream);
            reader.ReadLine();
            for (int idx = 0; idx < copy.Parameters.Length; idx++)
            {
                string input = reader.ReadLine().Split(',')[7];
                copy.Parameters[idx].Data = input;
            }
            return copy;
        }

        public static bool SendCommand(DecipheredPacket command, string guid)
        {
            return context.SendCommand(command, guid);
        }

        public static bool StreamCommand(DecipheredPacket command)
        {
            return context.StreamCommand(command);
        }

        public static IStorageElement AddTelecommand()
        {
            return context.AddTelecommand();
        }

        public static void ReseedId(uint id, bool telemetryOrTelecommand)
        {
            context.ReseedId(id, telemetryOrTelecommand);
        }
    }
}
