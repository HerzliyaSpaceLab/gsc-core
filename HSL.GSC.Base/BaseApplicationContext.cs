﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using HSL.GSC.Base.Serving;
using HSL.GSC.Base.Business.Managing;
using HSL.GSC.Base.Data.MissionInfromation;
using HSL.GSC.Base.Data.Storage;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Moq;
using HSL.GSC.CommunicationFormat.Packets;
using HSL.GSC.CommunicationFormat.Packets.Parameters;
using HSL.GSC.Base.Data.SPL;
using System.Configuration;

namespace HSL.GSC.Base
{
    class BaseApplicationContext : ApplicationContext
    {
        //private NotifyIcon trayIcon;
        //private BaseForm baseForm;
        private MissionInfromationController missionInformationController;
        private StorageController storageController;
        private Server server;
        private EndNodeManager endNodeManager;
        private GUIManager guiManager = null;

        public bool Disposed { get; private set; }

        internal BaseApplicationContext(bool debug, string pluginsLocation, string storageLocation, string missionInformationLocation, ushort serverPort,
            byte ackServiceType, byte ackServiceSubtype)
        {
            Disposed = false;

            /*if (!Directory.Exists(pluginsLocation))
                Directory.CreateDirectory(pluginsLocation);*/
            missionInformationController = new MissionInfromationController(missionInformationLocation);

            if (debug)
                storageController = new CsvController(storageLocation, missionInformationController); // @"C:\Users\Kiril\Desktop"
            else
                storageController = new DatabaseController(storageLocation); // @"|DataDirectory|\Database\Database.mdf"

            endNodeManager = new EndNodeManager(storageController, missionInformationController, ackServiceType, ackServiceSubtype);

            if (!debug)
                guiManager = new GUIManager(endNodeManager, missionInformationController, storageController as DatabaseController);

            server = new Server(endNodeManager, guiManager, serverPort);

            ActivateMainFunctionality();
            //TestPlugin(pluginsDirectoryPath, "img.png", 28, 228);

            Debug.WriteLine("@@@@@ ACTIVATED @@@@@");
        }

        internal DecipheredPacket FindCertainFormat(byte serviceType, byte serviceSubtype, bool telemetryOrTelecommand)
        {
            return missionInformationController.FindCertainPacket(serviceType, serviceSubtype, telemetryOrTelecommand);
        }

        internal bool SendCommand(DecipheredPacket command, string guid)
        {
            return endNodeManager.SendCommand(command, guid);
        }

        internal bool StreamCommand(DecipheredPacket command)
        {
            return endNodeManager.StreamCommand(command);
        }

        internal IStorageElement AddTelecommand()
        {
            var tc = new TelecommandPacket(0, 255, 255, DateTime.Now, new byte[0]);
            return storageController.AddTelecommandPacket(tc);
        }

        internal void ReseedId(uint id, bool telemetryOrTelecommand)
        {
            (storageController as DatabaseController).ReseedId(id, telemetryOrTelecommand);
        }

        void ActivateMainFunctionality()
        {
            server.Start();
            endNodeManager.Start();
            if (guiManager != null)
            {
                guiManager.Start();
            }
        }
        void AbortMainFunctionality()
        {
            if (guiManager != null) guiManager.Dispose();
            endNodeManager.Dispose();
            server.Dispose();
        }

        #region Random Packets Generator
        private void RandomGenerator()
        {
            Random rnd = new Random();
            int telemetryAmount = 600;
            int telecommandAmount = 20;
            Stopwatch stopwatch;

            var telemetrySatTimes = new List<DateTime>();
            for (int idx = 0; idx < telemetryAmount; idx++)
            {
                telemetrySatTimes.Add(new DateTime(2018, 11, 23, 16, 0, 0).AddSeconds(rnd.Next(60) * -1).AddMinutes(rnd.Next(60) * -1).AddHours(rnd.Next(24) * -1).AddDays(rnd.Next(91) * -1));
            }
            telemetrySatTimes.Sort((a, b) => a.CompareTo(b));

            for (int idx = 0; idx < telemetryAmount; idx++)
            {
                storageController.AddTelemetryPacket(
                    new Data.SPL.TelemetryPacket(GenerateRandomTelemetryPacket(3, 25, telemetrySatTimes[idx],
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(-20, 50) + rnd.NextDouble()),
                        (float)(rnd.Next(-20, 50) + rnd.NextDouble()),
                        (float)(rnd.Next(-20, 50) + rnd.NextDouble()),
                        (float)(rnd.Next(-20, 50) + rnd.NextDouble()),
                        (float)(rnd.Next(-20, 50) + rnd.NextDouble()),
                        (float)(rnd.Next(-20, 50) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble()),
                        (float)(rnd.Next(0, 20) + rnd.NextDouble())
                )));

                var time = rnd.Next(1, 1001);
                stopwatch = Stopwatch.StartNew();
                while (stopwatch.ElapsedMilliseconds <= time) ;
                stopwatch.Reset();
            }

            for (int idx = 0; idx < telecommandAmount; idx++)
            {
                storageController.AddTelecommandPacket(
                    new global::HSL.GSC.Base.Data.SPL.TelecommandPacket(GenerateRandomTelecommandPacket(3, 125,
                        (float)(rnd.Next(5000) + rnd.NextDouble()),
                        (float)(rnd.Next(5000) + rnd.NextDouble())
                )));

                var time = rnd.Next(1, 1001);
                stopwatch = Stopwatch.StartNew();
                while (stopwatch.ElapsedMilliseconds <= time) ;
                stopwatch.Reset();
            }
        }
        private byte[] GenerateRandomTelemetryPacket(byte serviceType, byte serviceSubtype, DateTime satDateTime, params float[] values)
        {
            ushort length = (ushort)((10 + sizeof(float) * values.Length) & ushort.MaxValue);
            List<byte> pack = new List<byte>(new byte[]
            {
                    0x08,
                    0x0B,
                    0xC0,
                    0x00,
                    (byte)((length >> 0x08) & 0xFF),
                    (byte)(length & 0xFF),
            });
            List<byte> dataFieldWithoutCRC = new List<byte>(new byte[]
            {
                    0x10,
                    serviceType,
                    serviceSubtype,
            });
            dataFieldWithoutCRC.AddRange(DateTimeToBytes(satDateTime));
            for (int idx = 0; idx < values.Length; idx++)
            {
                dataFieldWithoutCRC.AddRange(BitConverter.GetBytes(values[idx])); // Source Data
            }
            pack.AddRange(dataFieldWithoutCRC);
            ushort crc = CRCCalculation(dataFieldWithoutCRC.ToArray());
            pack.AddRange(new byte[] { (byte)((crc >> 8) & 0xFF), (byte)(crc & 0xFF) }); // CRC

            return pack.ToArray();
        }
        private DecipheredPacket GenerateRandomValues(DecipheredPacket packetFormat)
        {
            Random rnd = new Random();
            DecipheredPacket copy = new DecipheredPacket(packetFormat);
            foreach (var parameter in packetFormat.Parameters)
            {
                string value = null;
                if (parameter is BitmapParameter)
                {
                }
                if (parameter is ByteParameter)
                {
                }
                if (parameter is DateTimeParameter)
                {
                }
                else if (parameter is DoubleParameter)
                {
                }
                else if (parameter is FloatParameter)
                {
                }
                else if (parameter is IntParameter)
                {
                }
                else
                {
                    throw new ArgumentException("Parameter is not supported");
                }
                parameter.Data = value;
            }
            return copy;
        }
        private TelemetryPacket DecipheredTelemetryPacketToDatabase(DecipheredPacket packet)
        {
            return new TelemetryPacket(packet.ServiceType, packet.ServiceSubtype, packet.SatDateTime, (byte[])packet.TotalRawData.Clone());
        }
        private byte[] GenerateRandomTelecommandPacket(byte serviceType, byte serviceSubtype, params float[] values)
        {
            ushort length = (ushort)((5 + sizeof(float) * values.Length) & ushort.MaxValue);
            List<byte> pack = new List<byte>(new byte[]
            {
                    0x18,
                    0x0B,
                    0xC0,
                    0x00,
                    (byte)((length >> 0x08) & 0xFF),
                    (byte)(length & 0xFF),
            });
            List<byte> dataFieldWithoutCRC = new List<byte>(new byte[]
            {
                    0x1F,
                    serviceType,
                    serviceSubtype,
            });
            for (int idx = 0; idx < values.Length; idx++)
            {
                dataFieldWithoutCRC.AddRange(BitConverter.GetBytes(values[idx])); // Source Data
            }
            pack.AddRange(dataFieldWithoutCRC);
            ushort crc = CRCCalculation(dataFieldWithoutCRC.ToArray());
            pack.AddRange(new byte[] { (byte)((crc >> 8) & 0xFF), (byte)(crc & 0xFF) }); // CRC
            return pack.ToArray();
        }
        private ushort CRCCalculation(byte[] inputData)
        {
            const ushort generator = 0x1021;
            ushort crc = 0xFFFF;
            foreach (byte b in inputData)
            {
                crc ^= (ushort)(b << 8);
                for (int idx = 0; idx < 8; idx++)
                {
                    if ((crc & 0x8000) != 0)
                    {
                        crc = (ushort)((crc << 1) ^ generator);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            return crc;
        }
        private byte[] DateTimeToBytes(DateTime time)
        {
            DateTime startDate = new DateTime(2000, 1, 1);
            TimeSpan timeWithoutCrap = time - startDate;
            uint secondsUInt = Convert.ToUInt32(Math.Floor(timeWithoutCrap.TotalSeconds));
            byte[] timeBytes = new byte[5];
            for (int idx = 0; idx < 4; idx++)
            {
                timeBytes[3 - idx] = (byte)((secondsUInt >> 8 * idx) % 256);
            }
            timeBytes[4] = (byte)Math.Round((double)(timeWithoutCrap.Milliseconds * 256) / 1000);
            return timeBytes;
        }
        #endregion

        #region Plugins Games
        /*void TestPlugin(string pluginsDirectoryPath, string testImagePath, int chunkIdx, int payloadIdx)
        {
            var watch = Stopwatch.StartNew();
            var plugins = new Business.Plugins.PluginsController(pluginsDirectoryPath);
            var bitmap = new Bitmap(testImagePath);
            var chunk = GetChunk(bitmap, chunkIdx);
            ImageConverter converter = new ImageConverter();
            List<byte> bytes = new List<byte>((byte[])converter.ConvertTo(chunk, typeof(byte[])));
            bytes.InsertRange(0, BitConverter.GetBytes((uint)payloadIdx).Reverse().ToArray());
            bytes.InsertRange(4, BitConverter.GetBytes((uint)chunkIdx).Reverse().ToArray());
            Mock<API.IRawTelemetry> tm = new Mock<API.IRawTelemetry>(MockBehavior.Strict);
            tm.Setup(x => x.Data).Returns(bytes.ToArray());
            tm.Setup(x => x.Time).Returns(DateTime.Now);
            Mock<DatabaseController> controller = new Mock<DatabaseController>(MockBehavior.Strict, null);
            Mock<EndNodeManager> manager = new Mock<EndNodeManager>(MockBehavior.Strict, controller.Object);
            byte[] image = manager.Object.DecipherAndSave(plugins.Plugins.First(), tm.Object);
            BytesToImage(image, bitmap.Width, bitmap.Height).Save("chunk.png", ImageFormat.Png);
            watch.Stop();
        }
        void TestPlugin(string pluginsDirectoryPath, string testImagePath, string chunkPath, int payloadIdx)
        {
            var plugins = new Business.Plugins.PluginsController(pluginsDirectoryPath);
            var bitmap = new Bitmap(testImagePath);
            var chunk = new Bitmap(chunkPath);
            int chunkIdx = 36;
            ImageConverter converter = new ImageConverter();
            List<byte> bytes = new List<byte>((byte[])converter.ConvertTo(chunk, typeof(byte[])));
            bytes.InsertRange(0, BitConverter.GetBytes((uint)payloadIdx).Reverse().ToArray());
            bytes.InsertRange(4, BitConverter.GetBytes((uint)chunkIdx).Reverse().ToArray());
            Mock<API.IRawTelemetry> tm = new Mock<API.IRawTelemetry>(MockBehavior.Strict);
            tm.Setup(x => x.Data).Returns(bytes.ToArray());
            tm.Setup(x => x.Time).Returns(DateTime.Now);
            Mock<EndNodeManager> manager = new Mock<EndNodeManager>(MockBehavior.Strict, null);
            byte[] image = manager.Object.DecipherAndSave(plugins.Plugins.First(), tm.Object);
            BytesToImage(image, 64, 64).Save("chunk.png", ImageFormat.Png);
        }
        Bitmap BytesToImage(byte[] data, int width, int height)
        {
            Bitmap bmp;
            using (var ms = new MemoryStream(data))
            {
                bmp = new Bitmap(ms);
            }
            return bmp;

            /*int currentStride = width;
            int newStride = width;
            byte[] newBytes = new byte[newStride * height];
            for (int idx = 0; idx < height; idx++)
                Buffer.BlockCopy(data, currentStride * idx, newBytes, newStride * idx, currentStride);

            return new Bitmap(width, height, 3 * width, PixelFormat.Format24bppRgb,
                Marshal.UnsafeAddrOfPinnedArrayElement(newBytes, 0));*/
        /*}
        Bitmap GetChunk(Bitmap image, int chunkIndex)
        {
            Bitmap chunk = new Bitmap(64, 64);
            int imageWidth = image.Width, imageHeight = image.Height, chunkWidth = chunk.Width, chunkHeight = chunk.Height;
            int numChunkInRow = imageWidth / chunkWidth;
            int numChunkInColumn = imageHeight / chunkHeight;
            int row = (chunkIndex - 1) / numChunkInRow;
            int column = (chunkIndex - 1) % numChunkInRow;
            for (int ydx = chunkHeight * row; ydx < chunkHeight * (row + 1); ydx++)
            {
                int chunkYdx = ydx - chunkHeight * row;
                for (int xdx = chunkWidth * column; xdx < chunkWidth * (column + 1); xdx++)
                {
                    int chunkXdx = xdx - chunkWidth * column;
                    chunk.SetPixel(chunkXdx, chunkYdx, image.GetPixel(xdx, ydx));
                }
            }
            return chunk;
        }*/
        #endregion

        #region Tray and Old Plugins
        /*void ActivateTrayFormAndPlugins(string pluginsDirectoryPath)
        {
            trayIcon = new NotifyIcon();
            //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            //trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            trayIcon.Icon = Properties.Resources.GUITCPClient;
            trayIcon.Text = "GSC-Base";
            trayIcon.MouseDoubleClick += new MouseEventHandler(TrayIcon_MouseDoubleClick);
            trayIcon.Visible = false;
            trayIcon.ContextMenu = new ContextMenu(new MenuItem[]
                {
                    new MenuItem("Exit", new EventHandler(TrayIcon_Exit))
                });

            PluginsController.Initialize(pluginsDirectoryPath);
            baseForm = new BaseForm(pluginsDirectoryPath);
            //PluginsController.Instance.Plugins.CollectionChanged += PluginsCollectionChanged;
            //baseForm.UpdatePluginsListbox();
            var d = PluginsController.Instance.Plugins;
            baseForm.SetPluginsListbox(PluginsController.Instance.Plugins.Select(x => x.AssemblyDirectory + ": " + x.Name));
            baseForm.pluginsLocationBrowseButton.Click += new EventHandler(PluginsLocationBrowseButton_Click);

            trayIcon.Visible = true;
            trayIcon.ShowBalloonTip(1000, "GSC-Base", "At your service", ToolTipIcon.Info);
            baseForm.Show();
        }
        void PluginsLocationBrowseButton_Click(object sender, EventArgs e)
        {
            baseForm.ClearPluginsListbox();
            PluginsController.Reinitialize(Properties.Settings.Default.PluginLocation);
            //PluginsController.Instance.Plugins.CollectionChanged += PluginsCollectionChanged;
            //baseForm.UpdatePluginsListbox();
            baseForm.SetPluginsListbox(PluginsController.Instance.Plugins.Select(x => x.AssemblyDirectory + ": " + x.Name));
        }
        void PluginsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs args)
        {
            baseForm.UpdatePluginsListbox();
        }
        void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            baseForm.Show();
            baseForm.WindowState = FormWindowState.Normal;
        }
        void TrayIcon_Exit(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to close the GSC-Base?",
                "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                trayIcon.ShowBalloonTip(1000, "GSC-Base", "Out of service", ToolTipIcon.Info);
                trayIcon.Visible = false;
                AbortMainFunctionality();
                PluginsController.Instance.Dispose();
                Application.Exit();
            }
        }*/
        #endregion

    }
}
