﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using HSL.GSC.Base.Business.Managing;
using HSL.GSC.Base.Business.ClientHandling;
using System.Runtime.Serialization.Formatters.Binary;
using HSL.GSC.CommunicationFormat;
using System.Threading;

namespace HSL.GSC.Base.Serving
{
    /// <summary>
    /// The singleton class of the TCP server
    /// </summary>
    public sealed class Server : IDisposable
    {
        private static BinaryFormatter formatter = new BinaryFormatter();
        /// <summary>
        /// The parameter thats stops the threads in a call
        /// </summary>
        private volatile bool disposed = false;
        /// <summary>
        /// The TCP server
        /// </summary>
        private TcpListener tcpServer;
        private EndNodeManager endNodeManager;
        private GUIManager guiManager;
        private Task serverTask;
        private object acceptanceLock = new object();

        /// <summary>
        /// The main constructor
        /// </summary>
        public Server(EndNodeManager endNodeManager, GUIManager guiManager, ushort port)
        {
            this.endNodeManager = endNodeManager;
            this.guiManager = guiManager;
            tcpServer = new TcpListener(IPAddress.Parse("0.0.0.0"), port);
        }

        public void Start()
        {
            tcpServer.Start();
            serverTask = ConnectionHandling();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (serverTask != null)
                {
                    disposed = true;
                    serverTask.Wait();
                    serverTask.Dispose();
                    serverTask = null;
                }
                if (tcpServer != null)
                {
                    tcpServer.Stop();
                    tcpServer = null;
                }
                formatter = null;
                endNodeManager = null;
                guiManager = null;
                acceptanceLock = null;
            }
        }
        ~Server()
        {
            Dispose(false);
        }
        /// <summary>
        /// Starts the serving of TCP clients
        /// </summary>
        private async Task ConnectionHandling()
        {
            if (disposed) throw new InvalidOperationException();
            while (!disposed)
            {
                if (disposed) continue;
                TcpClient tcpClient = null;
                try
                {
                    tcpClient = await tcpServer.AcceptTcpClientAsync();
                }
                catch (SocketException ex)
                {
                    continue;
                }
                lock (acceptanceLock)
                {
                    ClientHandler client = null;
                    try
                    {
                        client = new ClientHandler(tcpClient);
                    }
                    catch (TimeoutException ex)
                    {
                        client.Dispose();
                        tcpClient.Dispose();
                        continue;
                    }

                    if (client.ClientIdentity.Type == ClientIdentity.ClientType.GUI)
                    {
                        if (guiManager != null) guiManager.AddClient(client);
                    }
                    else if (client.ClientIdentity.Type == ClientIdentity.ClientType.Controller)
                    {
                        if (guiManager != null)
                        {
                            if (guiManager.IsThereController)
                                client.EnqueueOutgoing(new ExternalMessage(MessageTypeEnum.SystemAck, new SystemAck(SystemAck.SystemAckType.ManagerKick, "The manager kicked you off")));
                            else
                                guiManager.AddClient(client);
                        }
                    }
                    else
                    {
                        endNodeManager.AddClient(client);
                    }
                }
            }
        }
    }
}
