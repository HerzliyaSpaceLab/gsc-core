﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace HSL.GSC.API
{
    public interface IPlugin
    {
        string Name { get; }
        string AssemblyDirectory
        {
            get;
            /*{
                return GetType().Assembly.CodeBase;
                //return (new Uri(GetType().Assembly.CodeBase)).LocalPath;
            }*/
        }
        string AssemblyName
        {
            get;
            /*{
                return GetType().Assembly.GetName().Name;
            }*/
        }
        byte ServiceType { get; }
        byte ServiceSubtype { get; }
        /*protected IPlugin(string name)
        {
            this.name = name;
        }*/
        IDatabaseData DecipherAndRequire(IRawTelemetry telemetry, out IDecipheredPart[] parts);
        IDatabaseData InsertParts(IDatabaseData data, IDecipheredPart[] parts);
    }
}
