﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.API
{
    public interface IDatabaseData
    {
        bool TelemetryOrTelecommand { get; }
        byte ServiceType { get; }
        byte ServiceSubtype { get; }
        uint PayloadId { get; }
        byte[] Data { get; }
    }
}
