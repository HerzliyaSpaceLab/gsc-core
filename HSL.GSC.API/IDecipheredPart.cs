﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.API
{
    public interface IDecipheredPart
    {
        uint InterID { get; }
        byte[] Data { get; }
    }
}
