﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSL.GSC.API
{
    public interface IRawTelemetry
    {
        byte[] Data { get; }
    }
}
